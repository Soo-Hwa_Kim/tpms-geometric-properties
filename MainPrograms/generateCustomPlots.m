function generateCustomPlots(surfaces, singleordouble, geometric_property1, geometric_property2, operation)
% generateCustomPlots(surfaces, singleordouble, geometric_property1, geometric_property2, operation)
%----------------------------------------------------------------------------
% generateCustomPlots.m is a top-level function. The user can create
% customized plots based on the geometric data created by TPMS_Analysis.m.
% These customized plots can combine geometric properties (ex. surface area
% to volume ratio) and can plot more than one type of TPMS on the same
% plot.
%
% Inputs:    surfaces                   cell of char -
%                                       alias for desired surface:
%                                           'Primitive'
%                                           'Gyroid'
%                                           'Diamond'
%                                           'Neovius'
%                                           'iWP'
%           singleordouble              numeric or string or char  -
%                                       'single' or 'double' surface
%                                       1 or 2
%           geometric_property1         char - 'Volume', 'SurfaceArea' or
%                                       'Thickness'
%           geometric_property2         char - 'Volume', 'SurfaceArea' or
%                                       'Thickness' to be combined with
%                                       geometric_property1
%           operation                   char - operator to combine the
%                                       properties. 'multiply' or 'divide'
%
%
% Outputs:   Contour plots (pdf and fig files) or line plots (pdf and fig files)
%            for non-normalized and normalized (range between [0, 1]) data
%
% Examples:  generateCustomPlots({'Primitive'}, 2, 'SurfaceArea', 'Volume', 'divide')
%            generateCustomPlots({'Primitive', 'Gyroid', 'Diamond', 'Neovius', 'iWP'}, ...
%            'single', 'Thickness')
%----------------------------------------------------------------------------

% Create (if not already) output directory

plot_full_dir = createCustomPlotDirectory;


% Create output plot filenames (for both pdfs and figs)

if nargin > 3
    
    [fig_name, pdf_name, fig_norm_name, pdf_norm_name, singleordouble] = customPlotFilenames(singleordouble, surfaces, geometric_property1, geometric_property2, operation);

else
    
    [fig_name, pdf_name, fig_norm_name, pdf_norm_name, singleordouble] = customPlotFilenames(singleordouble, surfaces, geometric_property1);

end

% If plotting more than one type of surfaces then start figure outside loop

if length(surfaces) > 1
    
    plot_colors = {'b-.', 'r:', 'c-', 'm>-', 'g--'};
    
    offset_max = 0;
    
    offset_min = 0;
    
    fig1 = figure; hold on
    
    
end


% Y axis labels for single surfaces
if nargin > 3
    
    y_axis_label = customYAxis(geometric_property1, operation, geometric_property2);

else

    y_axis_label = customYAxis(geometric_property1);
    
end

% Loop through each surface, prepare data, and plot!

for n = 1:length(surfaces)
    
    alias = surfaces{n};
    
    
    % Prepare data set 1
    
    geometric_data = getRawData(alias, geometric_property1, singleordouble);
    
    if singleordouble == 1
        
        [geometric_data(:,2), ~] = polyFitRawData(geometric_data(:,1), geometric_data(:,2));
        
    else
        
        geometric_data_cell1 = setupContourPlotInputs(geometric_data, geometric_property1);
        
        geometric_data_linear1 = [geometric_data_cell1{1}(:), geometric_data_cell1{2}(:), geometric_data_cell1{3}(:)];
        
        negative_same_offset_idxs1 = find(abs(geometric_data_linear1(:,1)) == abs(geometric_data_linear1(:, 2)));
        
        geometric_data = geometric_data_linear1(negative_same_offset_idxs1, :);
        
        geometric_data = geometric_data(~isnan(geometric_data(:,3)), :);
        
    end
    
    
    % Prepare data set 2 if user specified two geometric properties
    
    if nargin > 3
        
        geometric_data2 = getRawData(alias, geometric_property2, singleordouble);
        
        if singleordouble == 1
            
            [geometric_data2(:,2), ~] = polyFitRawData(geometric_data2(:,1), geometric_data2(:,2));
            
        else
            
            geometric_data_cell2 = setupContourPlotInputs(geometric_data2, geometric_property2);
            
            geometric_data_linear2 = [geometric_data_cell2{1}(:), geometric_data_cell2{2}(:), geometric_data_cell2{3}(:)];
            
            negative_same_offset_idxs2 = find(abs(geometric_data_linear2(:,1)) == abs(geometric_data_linear2(:, 2)));
            
            geometric_data2 = geometric_data_linear2(negative_same_offset_idxs2, :);
            
            geometric_data2 = geometric_data2(~isnan(geometric_data2(:,3)), :);
            
        end
        
        
        % Perform operation for combining geometric properties if
        % applicable
        
        if contains(operation, 'ult')
            
            if singleordouble == 1
                
                geometric_data(:,2) = geometric_data(:,2) .* geometric_data2(:,2);
                
            else
                geometric_data(:,3) = geometric_data(:,3) .* geometric_data2(:,3);
                
            end
            
        elseif contains(operation, 'iv')
            
            if singleordouble == 1
                
                geometric_data(:,2) = geometric_data(:,2) ./ geometric_data2(:,2);
                
            else
                
                geometric_data(:,3) = geometric_data(:,3) ./ geometric_data2(:,3);
                
            end
            
        end
        
    end
    
    
    % Plot
    
    if length(surfaces) > 1
        
        if singleordouble == 1
            
            offset_max = max(offset_max, max(geometric_data(:,1)));
            offset_min = min(offset_min, min(geometric_data(:,1)));
            
            plot(geometric_data(:,1), geometric_data(:,2), plot_colors{n}, 'LineWidth', 2, 'MarkerSize', 1)
            xlim([offset_min, offset_max])
            ylim([0 inf])
            axv = gca;
            axv.FontSize = 7;
            xlabel('Isovalue', 'interpreter', 'latex', 'FontWeight', 'bold')
            ylabel(y_axis_label, 'interpreter', 'latex', 'FontWeight', 'bold')
            
        else
            
            offset_max = max(offset_max, max(geometric_data(:,1)));
            
            plot(geometric_data(:,1), geometric_data(:,3), plot_colors{n}, 'LineWidth', 2, 'MarkerSize', 1)
            xlim([0, offset_max])
            ylim([0 inf])
            axv = gca;
            axv.FontSize = 7;
            xlabel('Isovalue', 'interpreter', 'latex', 'FontWeight', 'bold')
            ylabel(y_axis_label, 'interpreter', 'latex', 'FontWeight', 'bold')
            
        end
        
    else % only one surface
        
        if singleordouble == 1
            
            fig1 = figure;
            plot(geometric_data(:,1), geometric_data(:,2), 'k', 'LineWidth', 2)
            xlim([geometric_data(1,1) geometric_data(end,1)])
            ylim([min(geometric_data(:,2)) max(geometric_data(:,2))])
            axv = gca;
            axv.FontSize = 7;
            xlabel('Isovalue', 'interpreter', 'latex', 'FontWeight', 'bold')
            ylabel(y_axis_label, 'interpreter', 'latex', 'FontWeight', 'bold')
            
        else
            
            fig1 = figure;
            plot(geometric_data(:,1), geometric_data(:,3), 'k', 'LineWidth', 2)
            xlim([0, geometric_data(end,1)])
            ylim([min(geometric_data(:,2)) max(geometric_data(:,2))])
            axv = gca;
            axv.FontSize = 7;
            xlabel('Isovalue', 'interpreter', 'latex', 'FontWeight', 'bold')
            ylabel(y_axis_label, 'interpreter', 'latex', 'FontWeight', 'bold')
            
        end
        
        
    end
    
    
    
end


% Add legend after loop if there are more than one specified surfaces

if length(surfaces) > 1
    
    legend(surfaces{:}, 'Interpreter', 'latex', 'Location', 'northeastoutside')
    
end


% save plots as pdfs and figs

savefig([plot_full_dir, fig_name]) %save as fig
set(fig1, 'Units', 'Inches');
pos = get(fig1, 'Position');
set(fig1, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
print(fig1, [plot_full_dir, pdf_name], '-dpdf', '-r0') %save as pdf


% Loop through each surface, prepare data, and plot normalized data!

fig2 = figure; hold on

for n = 1:length(surfaces)
    
    alias = surfaces{n};
    
    % Prepare data set 1
    
    geometric_data = getRawData(alias, geometric_property1, singleordouble);
    
    if singleordouble == 1
        
        [geometric_data(:,2), ~] = polyFitRawData(geometric_data(:,1), geometric_data(:,2));
        
    else
        
        geometric_data_cell1 = setupContourPlotInputs(geometric_data, geometric_property1);
        
        geometric_data_linear1 = [geometric_data_cell1{1}(:), geometric_data_cell1{2}(:), geometric_data_cell1{3}(:)];
        
        negative_same_offset_idxs1 = find(abs(geometric_data_linear1(:,1)) == abs(geometric_data_linear1(:, 2)));
        
        geometric_data = geometric_data_linear1(negative_same_offset_idxs1, :);
        
        geometric_data = geometric_data(~isnan(geometric_data(:,3)), :);
        
    end
    
    
    % Prepare data set 2 if user specified two geometric properties
    
    if nargin > 3
        
        geometric_data2 = getRawData(alias, geometric_property2, singleordouble);
        
        if singleordouble == 1
            
            [geometric_data2(:,2), ~] = polyFitRawData(geometric_data2(:,1), geometric_data2(:,2));
            
        else
            
            geometric_data_cell2 = setupContourPlotInputs(geometric_data2, geometric_property2);
            
            geometric_data_linear2 = [geometric_data_cell2{1}(:), geometric_data_cell2{2}(:), geometric_data_cell2{3}(:)];
            
            negative_same_offset_idxs2 = find(abs(geometric_data_linear2(:,1)) == abs(geometric_data_linear2(:, 2)));
            
            geometric_data2 = geometric_data_linear2(negative_same_offset_idxs2, :);
            
            geometric_data2 = geometric_data2(~isnan(geometric_data2(:,3)), :);
            
        end
        
        
        % Perform operation for combining geometric properties if
        % applicable
        
        if contains(operation, 'ult')
            
            if singleordouble == 1
                
                geometric_data(:,2) = geometric_data(:,2) .* geometric_data2(:,2);
                
            else
                geometric_data(:,3) = geometric_data(:,3) .* geometric_data2(:,3);
                
            end
            
        elseif contains(operation, 'iv')
            
            if singleordouble == 1
                
                geometric_data(:,2) = geometric_data(:,2) ./ geometric_data2(:,2);
                
            else
                
                geometric_data(:,3) = geometric_data(:,3) ./ geometric_data2(:,3);
                
            end
            
        end
        
    end
    
    
    
    % Normalize offset values to range from [0 1]
    
    geometric_data = normalizeGeometricData(geometric_data, singleordouble);
    
    
    % Plot
    
    if length(surfaces) > 1
        
        if singleordouble == 1
            
            plot(geometric_data(:,1), geometric_data(:,2), plot_colors{n}, 'LineWidth', 2, 'MarkerSize', 1)
            xlim([-1, 1])
            ylim([0 inf])
            axv = gca;
            axv.FontSize = 7;
            xlabel('Normalized Isovalue', 'interpreter', 'latex', 'FontWeight', 'bold')
            ylabel(y_axis_label, 'interpreter', 'latex', 'FontWeight', 'bold')
            
        else
            
            plot(geometric_data(:,1), geometric_data(:,3), plot_colors{n}, 'LineWidth', 2, 'MarkerSize', 1)
            xlim([0, 1])
            ylim([0 inf])
            axv = gca;
            axv.FontSize = 7;
            xlabel('Normalized Isovalue', 'interpreter', 'latex', 'FontWeight', 'bold')
            ylabel(y_axis_label, 'interpreter', 'latex', 'FontWeight', 'bold')
            
        end
        
    else % only one surface
        
        if singleordouble == 1
            
            fig1 = figure;
            plot(geometric_data(:,1), geometric_data(:,2), 'k', 'LineWidth', 2)
            xlim([0, 1])
            ylim([min(geometric_data(:,2)) max(geometric_data(:,2))])
            axv = gca;
            axv.FontSize = 7;
            xlabel('Normalized Isovalue', 'interpreter', 'latex', 'FontWeight', 'bold')
            ylabel(y_axis_label, 'interpreter', 'latex', 'FontWeight', 'bold')
            
        else
            
            fig1 = figure;
            plot(geometric_data(:,1), geometric_data(:,2), 'k', 'LineWidth', 2)
            xlim([0, 1])
            ylim([min(geometric_data(:,2)) max(geometric_data(:,2))])
            axv = gca;
            axv.FontSize = 7;
            xlabel('Normalized Isovalue', 'interpreter', 'latex', 'FontWeight', 'bold')
            ylabel(y_axis_label, 'interpreter', 'latex', 'FontWeight', 'bold')
            
        end
        
        
    end
    

end


% Add legend after loop if there are more than one specified surfaces

if length(surfaces) > 1
    
    legend(surfaces{:}, 'Interpreter', 'latex', 'Location', 'northeastoutside')
    
end


% save plots as pdfs and figs

savefig([plot_full_dir, fig_norm_name]) %save as fig
set(fig2, 'Units', 'Inches');
pos = get(fig2, 'Position');
set(fig2, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
print(fig2, [plot_full_dir, pdf_norm_name], '-dpdf', '-r0') %save as pdf


end


function raw_data = getRawData(alias, geometric_property, singleordouble)
% Retrieve raw data


if contains(geometric_property, 'hickness')
    
    if singleordouble == 1
        
        fname = [alias, 'RawStrut', geometric_property, 'Data.txt'];
        
    else
        
        fname = [alias, 'RawDoubleSurface', geometric_property, 'Data.txt'];
        
    end
    
    
else
    
    fname = [alias, 'Raw', geometric_property, 'Data.txt'];
    
end


raw_data = dlmread(fname);

raw_data = checkOffsetRange(raw_data);


end

function geometric_data = normalizeGeometricData(geometric_data, singleordouble)
% Normalize geometric data to range of [0, 1]

if singleordouble == 1
    
    if abs(min(geometric_data(:, 1))) ~= abs(max(geometric_data(:, 1)))
        
        limit = min(abs(min(geometric_data(:, 1))), abs(max(geometric_data(:, 1))));
        
        geometric_data = geometric_data(find(geometric_data(:, 1) == -limit) : find(geometric_data(:, 1) == limit), :);
        
        
    end
    
end

% If single surface have the normalised range go from [-1 1] and if double
% surface go from [0 1]. This is so it is less confusing when looking at
% plots without having read in depth what is going on. This reflects that
% the double surface plots have half the range as the single surface plots
% but they are both normalised.
if singleordouble == 1
    
    geometric_data(:, 1) = linspace(-1, 1, length(geometric_data(:, 1)));
    
else
    
    geometric_data(:, 1) = linspace(0, 1, length(geometric_data(:, 1)));
    
end


end


