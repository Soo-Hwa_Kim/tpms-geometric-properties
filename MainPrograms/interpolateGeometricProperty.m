function interpolated_value = interpolateGeometricProperty(surface, singleordouble, value, input_property, output_property)
% interpolated_value = interpolateGeometricProperty(surface, singleordouble, value, input_property, output_property)
%----------------------------------------------------------------------------
% interpolateGeometricProperty.m is a top-level function. The user can
% interpolate values from the geometric data from TPMS_Analysis.m. User
% specifies which dataset they would like to use, what specific condition,
% and what they want to interpolate. Example: For single surface Primitive
% interpolate volume value given a specific offset value. This works vice
% versa as well. If value is beyond range (i.e. extrapolated) output will 
% be NaN. This is a simple linear interpolation not necessarily to be used 
% for it's values but this program can be a potentially be a starting 
% point for future work.
%
% Inputs:    surfaces                   char - alias for desired surface:
%                                           'Primitive'
%                                           'Gyroid'
%                                           'Diamond'
%                                           'Neovius'
%                                           'iWP'
%           singleordouble              char  - 'single' or 'double' 
%                                       surface
%           value                       numeric - input value. If double
%                                       surface there might be a need for 
%                                       two input values as an array
%           input property              char - 'Volume', 'SurfaceArea',
%                                       'Thickness', or 'Offset'
%           output property             char - 'Volume', 'SurfaceArea',
%                                       'Thickness', or 'Offset'
%            
%
% Outputs:  interpolated_value          numeric - single or array of values
%
% Example:  generateCustomPlots('Primitive', 'single', 0.512, 'Volume', 'offset')
%----------------------------------------------------------------------------


% Prepare data

all_data = importAllData(surface);

if contains(singleordouble, 'oubl')
    
    all_data = getDoubleSurfaceData(all_data);
    
end

specified_data = getSpecificData(all_data, input_property, output_property, singleordouble);


% Interpolate data depending on what we are interpolating

if contains(input_property, 'ff')
    
    
    if contains(singleordouble, 'ingle')
        
        interpolated_value = interp1(specified_data(:,1), specified_data(:,2), value);
        
    else
        
        interpolated_value = interp2(specified_data{:}, value(1), value(2));
        
    end
    
    
else
    
    
    if contains(singleordouble, 'ingle')
        
        if contains(input_property, 'urface')
            
            interpolated_value = interp1(specified_data(1:ceil(length(specified_data)/2),2), specified_data(1:ceil(length(specified_data)/2),1), value);
            
        else
            
            interpolated_value = interp1(specified_data(:,2), specified_data(:,1), value);
            
        end
        
    else
        
        interpolated_value = contour(specified_data{:}, [value, value]);
        
        interpolated_value = interpolated_value(:, 2:end);
        
    end
    
    
end


end



function all_data = importAllData(surface)


all_data.VolumeSingle = dlmread([surface,'RawVolumeData.txt']);

all_data.SurfaceAreaSingle = dlmread([surface,'RawSurfaceAreaData.txt']);

all_data.ThicknessStrut = dlmread([surface,'RawStrutThicknessData.txt']);

all_data.ThicknessDouble = dlmread([surface,'RawDoubleSurfaceThicknessData.txt']);


end



function all_data = getDoubleSurfaceData(all_data)


offsets = all_data.VolumeSingle(:,1);

combos = nchoosek(offsets, 2);

volume_data = zeros(length(combos), 1);

surface_area_data = zeros(length(combos), 1);


for n = 1:length(combos)
    
    offset1 = combos(n,1);
    
    offset2 = combos(n,2);
    
    row1 = find(offsets(:, 1) == offset1);
    
    row2 = find(offsets(:, 1) == offset2);
    
    volume_data(n) = abs(all_data.VolumeSingle(row1, 2) - all_data.VolumeSingle(row2, 2));
    
    surface_area_data(n) = all_data.SurfaceAreaSingle(row1, 2) + all_data.SurfaceAreaSingle(row2, 2);
    
end

volume2d = makeData2D([combos volume_data]);

surfacearea2d = makeData2D([combos surface_area_data]);

thickness2d = makeData2D(all_data.ThicknessDouble);


all_data.VolumeDouble = [combos volume_data];

all_data.Volume2D = volume2d;

all_data.SurfaceAreaDouble = [combos surface_area_data];

all_data.SurfaceArea2D = surfacearea2d;

all_data.Thickness2D = thickness2d;


end

function data2d = makeData2D(data)


offsets = unique(data(:,1));

[x, y] = meshgrid(offsets, offsets);

z = nan(size(x));

for n = 1:size(data, 1)
    
    data_n = data(n, :);
    
    ind1 = find(offsets == data_n(1));
    
    ind2 = find(offsets == data_n(2));
    
    z(ind1,ind2) = data_n(3);
    
    
end

data2d = {x, y, z};


end


function specified_data = getSpecificData(all_data, input_property, output_property, singleordouble)


if contains(singleordouble, 'ingle')
    
    if contains(output_property, 'olume') || contains(input_property, 'olume')
        
        specified_data = all_data.VolumeSingle;
        
    elseif contains(output_property, 'urface') || contains(input_property, 'urface')
        
        specified_data = all_data.SurfaceAreaSingle;
        
    elseif contains(output_property, 'hickness') || contains(input_property, 'hickness')
        
        specified_data = all_data.ThicknessStrut;
        
        
    end
    
    
else
    
    if contains(output_property, 'olume') || contains(input_property, 'olume')
        
        specified_data = all_data.Volume2D;
        
    elseif contains(output_property, 'urface') || contains(input_property, 'urface')
        
        specified_data = all_data.SurfaceArea2D;
        
    elseif contains(output_property, 'hickness') || contains(input_property, 'hickness')
        
        specified_data = all_data.Thickness2D;
        
        
    end
    
    
end


end