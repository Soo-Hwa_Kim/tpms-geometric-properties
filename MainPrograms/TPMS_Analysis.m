function TPMS_Analysis(varargin)
% TPMS_Analysis(varargin)
%----------------------------------------------------------------------------
% TPMS_Analysis.m is the top-level function. User will input
% triply-periodic surface function alias(es) for analysis. The analysis
% calculates the volume, surface area, and thickness of each solid surface
% strut and double surfaces for a predetermined set of offsets. Analysis
% will output raw data as txt files and plots as pdf (for documentation)
% and fig files (for interactive purposes). The raw data is only generated
% if the txt file doesn't exist in the correct directory. The plots are
% saved into output directories and the automatic visualization is
% surpressed. If user has put inputs in incorrectly or nothing at all,
% the default is to run TPMS_Analysis.m for all five TPMS.
%
% Inputs:    varargin           char - alias for desired surface:
%                                      'Primitive'
%                                      'Gyroid'
%                                      'Diamond'
%                                      'Neovius'
%                                      'iWP'
%
% Outputs:   Raw data (txt file), polynomial fitting coefficients data(txt 
%            file), contour plots (pdf and fig files), and line plots (pdf 
%            and fig files)
%
% Examples:  TPMS_Analysis('Primitive')
%            TPMS_Analysis('Primitive', 'Gyroid', 'Diamond', 'Neovius', 'iWP')
%----------------------------------------------------------------------------

%% Parse inputs

aliases = parseInputs(varargin);


%% Loop through all input surface aliases

for n = 1:length(aliases)
    
    alias = aliases{n};
    
    
    %% Set up TPMS information
    
    % TPMS information for each offset is stored in the structure
    % SURFACE_DATA to be used to calculate each of the geometric
    % properties
    
    % Store alias and root directory
    
    SURFACE_DATA = setupSurfaceDataStructure(alias);
    
    
    % Prepare TPMS data within structure
    
    SURFACE_DATA = prepareTPMSData(SURFACE_DATA);
    
    
    %% Generate Raw Data for Geometric Properties
    
    if SURFACE_DATA.flags.generate_data_flag
                
        % Volume
        
        if SURFACE_DATA.flags.calculate_volume_flag
            
            generateVolumeData(SURFACE_DATA);
            
        end
        
        
        % Surface Area
        
        if SURFACE_DATA.flags.calculate_surface_area_flag
            
            generateSurfaceAreaData(SURFACE_DATA);
            
        end
        
        
        % Minimum Thickness
        
        if SURFACE_DATA.flags.calculate_thickness_flag
            
            generateThicknessData(SURFACE_DATA)
            
        end
        
        
    end
    
    
    %% Create visuals of the data
    
    setupAndGeneratePlots(SURFACE_DATA);
    
    
end


end