function dF = gradientPrimitiveFun(x, y, z)
% dF = gradientPrimitiveFun(x, y, z)
%----------------------------------------------------------------------------
% gradientPrimitiveFun uses the Schwartz primitive gradient function to 
% generate gradient values for input x, y, z values
%
% Inputs:     x             numeric - 3D meshgrid of X dimension
%             y             numeric - 3D meshgrid of Y dimension
%             z             numeric - 3D meshgrid of Z dimension
%
% Outputs:    dF            gradient data
%----------------------------------------------------------------------------

%% Calculate gradient data

dx = -2*pi * sin(2*pi*x);

dy = -2*pi * sin(2*pi*y);

dz = -2*pi * sin(2*pi*z);


% Output

dF = [dx; dy; dz];


% Just in case input x, y, z matrices are oriented nx3 rather than 3xn

if size(dF,1) ~= 3 || size(dF,2) ~= 3
    
    dF = [dx, dy, dz];
    
end


end