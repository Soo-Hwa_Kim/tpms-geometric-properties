function dF = gradientiWPFun(x, y, z)
% dF = gradientiWPFun(x, y, z)
%----------------------------------------------------------------------------
% gradientiWPFun uses the Schoen iWP gradient function to generate gradient
% values for input x, y, z values
%
% Inputs:     x             numeric - 3D meshgrid of X dimension
%             y             numeric - 3D meshgrid of Y dimension
%             z             numeric - 3D meshgrid of Z dimension
%
% Outputs:    dF            gradient data
%----------------------------------------------------------------------------

%% Calculate gradient data

ddx = -4*pi * (sin(2*pi*x) .* cos(2*pi*y) + sin(2*pi*x) .* cos(2*pi*z) - sin(4*pi*x));

ddy = -4*pi * (sin(2*pi*y) .* cos(2*pi*x) + sin(2*pi*y) .* cos(2*pi*z) - sin(4*pi*y));

ddz = -4*pi * (sin(2*pi*z) .* cos(2*pi*x) + sin(2*pi*z) .* cos(2*pi*y) - sin(4*pi*z));


% Output

dF = [ddx; ddy; ddz];


% Just in case input x, y, z matrices are oriented nx3 rather than 3xn

if size(dF,1) ~= 3 || size(dF,2) ~= 3
    
    dF = [ddx, ddy, ddz];
    
end


end