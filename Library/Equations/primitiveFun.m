function f = primitiveFun(x, y, z, t)
% f = primitiveFun(x, y, z, t)
%----------------------------------------------------------------------------
% primitiveFun uses the Schwarz primitive implicit surface function to 
% generate function data for input x, y, z meshgrids at t offset
%
% Inputs:     x             numeric - 3D meshgrid of x dimension
%             y             numeric - 3D meshgrid of y dimension
%             z             numeric - 3D meshgrid of z dimension
%             t             numeric - offset
%
% Outputs:    f             function data
%----------------------------------------------------------------------------

%% Calculate function data

% If no offset is specified, then offset is set to 0

if nargin < 4
    
  t = 0;
  
end


% Function

f = cos(2*pi*x) + cos(2*pi*y) + cos(2*pi*z) - t;

  
end