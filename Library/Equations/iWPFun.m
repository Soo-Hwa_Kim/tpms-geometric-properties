function f = iWPFun(x, y, z, t)
% f = iWPFun(x, y, z, t)
%----------------------------------------------------------------------------
% iWPFun uses the Schoen iWP implicit surface function to generate function
% data for input x, y, z meshgrids at t offset
%
% Inputs:     x             numeric - 3D meshgrid of x dimension
%             y             numeric - 3D meshgrid of y dimension
%             z             numeric - 3D meshgrid of z dimension
%             t             numeric - offset
%
% Outputs:    f             function data
%----------------------------------------------------------------------------

%% Calculate function data

% If no offset is specified, then offset is set to 0

if nargin < 4
    
  t = 0;
  
end


% Function    

f = 2 * (cos(2*pi*x) .* cos(2*pi*y) + ...
         cos(2*pi*y) .* cos(2*pi*z) + ...
         cos(2*pi*z) .* cos(2*pi*x)) - ...
         (cos(4*pi*x) + cos(4*pi*y) + cos(4*pi*z)) - t;

end