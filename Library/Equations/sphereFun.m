function f = sphereFun(x, y, z, R, C)
% f = sphereFun(x, y, z, R, C)
%----------------------------------------------------------------------------
% sphereFun uses the sphere implicit surface function to generate function
% data for input x, y, z meshgrids for R radius and C center point
%
% Inputs:     x             numeric - 3D meshgrid of x dimension
%             y             numeric - 3D meshgrid of y dimension
%             z             numeric - 3D meshgrid of z dimension
%             R             numeric - radius
%             C             numeric - center point
%
% Outputs:    f             function data
%----------------------------------------------------------------------------

%% Calculate function data

% If no radius and/or center point is specified, then center point is at 
% [0, 0, 0] and the radius is 0.5  

if nargin < 5
 
  C = [0, 0, 0];
  
  if nargin < 4
      
        R = 0.5;
        
  end
  
end


% Function

f = (x - C(1)) .^ 2 + (y - C(2)) .^ 2 + (z - C(3)) .^ 2 - R .^ 2;
  

end