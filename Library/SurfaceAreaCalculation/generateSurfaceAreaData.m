function generateSurfaceAreaData(SURFACE_DATA)
% generateSurfaceAreaData(SURFACE_DATA)
%----------------------------------------------------------------------------
% generateSurfaceAreaData.m is the overhead function for calculating 
% surface area for each offset and saving the surface area data into a 
% text file. This function is only called if raw data for surface area
% does not exist in correct directory.
%
% Inputs:    SURFACE_DATA           struct
%
% Outputs:   raw data               saved text file
%
% Called by: TPMS_Analysis.m
%----------------------------------------------------------------------------

%% Calculate surface area for each offset

% Setup and pre-allocate

offset_list = SURFACE_DATA.offset_list;

offset_field_names = fieldnames(SURFACE_DATA.data);

surface_area_matrix = zeros(length(offset_list), 2);


% Loop through each offset and calculate surface area

for n = 1:length(offset_list)
    
    offset = offset_list(n);
    
    offset_field_name = offset_field_names{n};
    
    faces = SURFACE_DATA.data.(offset_field_name).initial_faces;
    
    vertices = SURFACE_DATA.data.(offset_field_name).initial_vertices;
    
    correct_vertices_inputs = {SURFACE_DATA.fhandle, SURFACE_DATA.ghandle, offset};
    
    surface_area = convergeSurfaceArea(faces, vertices, correct_vertices_inputs);
    
    surface_area_matrix(n, :) = [offset, surface_area];
    
    
end


%% Save raw data into text file

filename = SURFACE_DATA.filenames.raw_data_surface_area;

data_dir = fullfile([SURFACE_DATA.raw_data_dir, filename]);

dlmwrite(data_dir, surface_area_matrix);


end