function surface_area = surfaceAreaTriangulation(faces, vertices)
% surface_area = surfaceAreaTriangulation(faces, vertices)
%----------------------------------------------------------------------------
% surfaceAreaTriangulation.m is calculates the surface area by estimating the
% surface with a triangulated mesh and totaling the areas of all triangle
% faces.
%
% Inputs:    faces                      numeric - nx3 matrix
%            vertices                   numeric - mx3 matrix
%
% Outputs:   surface_area               numeric
%
% Called by: convergeSurfaceArea.m
%----------------------------------------------------------------------------

%% Calculate surface area

a = vertices(faces(:, 2), :) - vertices(faces(:, 1), :);

b = vertices(faces(:, 3), :) - vertices(faces(:, 1), :);

c = cross(a, b, 2);

surface_area = 1/2 * sum(sqrt(sum(c.^2, 2)));


end