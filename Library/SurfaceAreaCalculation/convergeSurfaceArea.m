function surface_area = convergeSurfaceArea(faces, vertices, correct_vertices_inputs)
% surface_area = convergeSurfaceArea(faces, vertices, correct_vertices_inputs)
%----------------------------------------------------------------------------
% convergeSurfaceArea.m calculates a converged surface area. The surface
% area calculation is repeated with increasing number of vertices and
% triangle faces until the surface area value has converged to a tolerance.
%
% Inputs:    faces                      numeric - nx3 matrix
%            vertices                   numeric - mx3 matrix
%            correct_vertices_inputs    cell - {fhandle, ghandle, offset}
%
% Outputs:   surface_area               numeric
%
% Called by: generateSurfaceAreaData.m
%----------------------------------------------------------------------------

%% Calculate converged surface area value

% Setup and pre-allocate

iteration = 1;

max_iteration = 8;

surface_areas = zeros(max_iteration, 1);

test_results = false;


% While loop until surface area value has converged. There is a safety
% catch to return a NaN value if there are too many iterations. While this
% should not happen for the TPMS values in the set offset ranges this is
% just in case so the program does not throw and error for memory issues.

while ~test_results
    
    
    % Calculate surface area
    
    surface_areas(iteration) = surfaceAreaTriangulation(faces, vertices);
    
    
    % Check for convergence
    
    if iteration > 2
        
        [test_results, ~] = testConvergence(surface_areas((iteration-2):iteration));

        if test_results
            
            break
            
        end
        
    end
    
    
    % Safety catch
    
    if iteration > max_iteration
        
        surface_area = nan;
        
        return
        
    end
    
    
    % If has not passed convergence test increase number of vertices
    
    [faces, vertices] = createNewCorrectedVertices(vertices, faces, correct_vertices_inputs{:});
    
    iteration = iteration + 1;
    
    
end


% Output converged surface area value

surface_area = surface_areas(iteration);


end