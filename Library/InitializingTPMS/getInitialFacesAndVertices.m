function SURFACE_DATA = getInitialFacesAndVertices(SURFACE_DATA)
% SURFACE_DATA = getInitialFacesAndVertices(SURFACE_DATA)
%----------------------------------------------------------------------------
% getInitialFacesAndVertices.m calculates a preliminary set of initial
% faces and vertices for each offset values and stores within SURFACE_DATA.
% This is done using a course mesh, using the MATLAB isosurface function to
% get faces and vertices, and the vertices are corrected with
% Newton-Raphson method to more accurate values
%
% Inputs:    SURFACE_DATA           struct
%
% Outputs:   SURFACE_DATA           struct
%
% Called by: prepareTPMSData.m
%----------------------------------------------------------------------------

%% Offset list to loop through

offsets = SURFACE_DATA.offset_list;


%% Generate mesh of function values

% Mesh size

mesh = 15;

% Create x, y, z meshgrid for one period

range = linspace(-0.5, 0.5, mesh);

[x, y, z] = meshgrid(range, range, range);

% Corresponding function values

function_values = SURFACE_DATA.fhandle(x, y, z);


%% Loop through each offset

for n = 1 : length(offsets)
    
    offset = offsets(n);
    
    
    % Create fieldnames for offsets within SURFACE_DATA
    
    if sign(offset) == -1
        
        offset_field_name = ['n', num2str(abs(offset) * 100)];
        
    else
        
        offset_field_name = ['p', num2str(offset * 100)];
        
    end
    
    
    % Calculate function values, faces, and vertices
    
    [faces, initial_vertices] = isosurface(x, y, z, function_values, offset);
    
    
    % Correct vertices to more accurate values
    
    correct_vertices_inputs = {SURFACE_DATA.fhandle, SURFACE_DATA.ghandle, offset};
    
    corrected_vertices = correctVertices(initial_vertices, correct_vertices_inputs{:});
    
     
    % Store information into SURFACE_DATA structure
    
    SURFACE_DATA.data.(offset_field_name).initial_faces = faces;
    
    SURFACE_DATA.data.(offset_field_name).initial_vertices = corrected_vertices;
       
    
end


end