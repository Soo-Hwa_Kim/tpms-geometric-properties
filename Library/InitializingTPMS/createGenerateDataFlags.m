function SURFACE_DATA = createGenerateDataFlags(SURFACE_DATA)
% SURFACE_DATA = createGenerateDataFlags(SURFACE_DATA)
%----------------------------------------------------------------------------
% createGenerateDataFlags.m creates flags on whether raw data should be
% generated and are saved in the SURFACE_DATA structure. If the raw data
% file doesn't exist (in correct directory) it will be generated. Flags
% are initialized as false and then set to true if data file doesn't exist
%
% Inputs:    SURFACE_DATA           struct
%
% Outputs:   SURFACE_DATA           struct
%
% Called by: setupSurfaceDataStructure.m
%----------------------------------------------------------------------------

%% Check if raw data exists. If not then set the generate data flag to true

% Initialize flags as false

SURFACE_DATA.flags.generate_data_flag = false;

SURFACE_DATA.flags.calculate_volume_flag = false;

SURFACE_DATA.flags.calculate_surface_area_flag = false;

SURFACE_DATA.flags.calculate_thickness_flag = false;

SURFACE_DATA.flags.calculate_strut_thickness_flag = false;

SURFACE_DATA.flags.calculate_double_surface_thickness_flag = false;


% Set flags to true if certain raw data doesn't exist

if ~exist([SURFACE_DATA.raw_data_dir, SURFACE_DATA.filenames.raw_data_volume], 'file')
    
    SURFACE_DATA.flags.generate_data_flag = true;
    
    SURFACE_DATA.flags.calculate_volume_flag = true;
    
end


if ~exist([SURFACE_DATA.raw_data_dir, SURFACE_DATA.filenames.raw_data_surface_area], 'file')
    
    SURFACE_DATA.flags.generate_data_flag = true;
    
    SURFACE_DATA.flags.calculate_surface_area_flag = true;
    
end


if ~exist([SURFACE_DATA.raw_data_dir, SURFACE_DATA.filenames.raw_data_strut_thickness], 'file')
    
    SURFACE_DATA.flags.generate_data_flag = true;
    
    SURFACE_DATA.flags.calculate_thickness_flag = true;
    
    SURFACE_DATA.flags.calculate_strut_thickness_flag = true;
    
end


if ~exist([SURFACE_DATA.raw_data_dir, SURFACE_DATA.filenames.raw_data_double_surface_thickness], 'file')
    
    SURFACE_DATA.flags.generate_data_flag = true;
    
    SURFACE_DATA.flags.calculate_thickness_flag = true;
    
    SURFACE_DATA.flags.calculate_double_surface_thickness_flag = true;
    
end


end