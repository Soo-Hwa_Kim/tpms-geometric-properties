function SURFACE_DATA = setupSurfaceDataStructure(alias)
% SURFACE_DATA = setupSurfaceDataStructure(alias)
%----------------------------------------------------------------------------
% setupSurfaceDataStructure.m creates the SURFACE_DATA structure that
% carries surface information for calculating geometric properties and
% generating plots. This function will setup SURFACE_DATA with the alias,
% output directories, output filenames, and generate data flags
%
% Inputs:    alias                  string - alias for desired surface
%
% Outputs:   SURFACE_DATA           struct
%
% Called by: TPMS_Analysis.m
%----------------------------------------------------------------------------

%% Initialize structure with alias

SURFACE_DATA = struct();

SURFACE_DATA.alias = alias;


%% Save (and create if don't exist) directories for outputs

SURFACE_DATA = createAndStoreDirectories(SURFACE_DATA);


%% Save filenames of outputs

SURFACE_DATA = storeOutputFilenames(SURFACE_DATA);


%% Create generate data flags 
% Lets the program know what data to generate based on whether it exists 
% or not

SURFACE_DATA = createGenerateDataFlags(SURFACE_DATA);


end