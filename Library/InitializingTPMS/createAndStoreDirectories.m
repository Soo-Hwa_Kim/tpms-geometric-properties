function SURFACE_DATA = createAndStoreDirectories(SURFACE_DATA)
% SURFACE_DATA = createAndStoreDirectories(SURFACE_DATA)
%----------------------------------------------------------------------------
% createAndStoreDirectories.m stores the output directories in the 
% SURFACE_DATA structure. If the directory doesn't exist it will be 
% created and added to the path. Output directories are for raw data and 
% plots. They are further categorized into the respective TPMS
%
% Inputs:    SURFACE_DATA           struct
%
% Outputs:   SURFACE_DATA           struct
%
% Called by: setupSurfaceDataStructure.m
%----------------------------------------------------------------------------

%% Root directory

SURFACE_DATA.root_dir = pwd;


%% Directories for storing raw data
% Raw data is categorized by TPMS

raw_data_path = ['/Outputs/RawData/', SURFACE_DATA.alias, '/'];

SURFACE_DATA.raw_data_dir = [SURFACE_DATA.root_dir, raw_data_path];

if ~exist([SURFACE_DATA.raw_data_dir], 'dir')
    
    mkdir([SURFACE_DATA.raw_data_dir]);
    
    addpath([SURFACE_DATA.raw_data_dir]);
    
end


%% Directories for storing output plots 
% Categorized by TPMS. Outputs are saved in both pdf and fig formats

plot_path = ['/Outputs/Plots/', SURFACE_DATA.alias, '/'];

pdf_path = 'pdfs/';

fig_path = 'figs/';


SURFACE_DATA.plot_pdf_dir = [SURFACE_DATA.root_dir, plot_path, pdf_path];

SURFACE_DATA.plot_fig_dir = [SURFACE_DATA.root_dir, plot_path, fig_path];

if ~exist(SURFACE_DATA.plot_pdf_dir, 'dir')
    
    mkdir(SURFACE_DATA.plot_pdf_dir);
    
    mkdir(SURFACE_DATA.plot_fig_dir);

    addpath(SURFACE_DATA.plot_pdf_dir);
    
    addpath(SURFACE_DATA.plot_fig_dir);
    
    
end

%% Directories for storing polynomial fitting coefficients

coeff_path = ['/Outputs/Polyfit/FittingCoefficients/', SURFACE_DATA.alias, '/'];

SURFACE_DATA.coeff_dir = [SURFACE_DATA.root_dir, coeff_path];

if ~exist(SURFACE_DATA.coeff_dir, 'dir')
    
    mkdir(SURFACE_DATA.coeff_dir);

    addpath(SURFACE_DATA.coeff_dir);
    
end


%% Directory and file for storing polynomial fitting tolerance logs

polyfit_tol_path = '/Outputs/Polyfit/ToleranceLog/';

SURFACE_DATA.polyfit_tol_dir = [SURFACE_DATA.root_dir, polyfit_tol_path];

if ~exist(SURFACE_DATA.polyfit_tol_dir, 'dir')
    
    mkdir(SURFACE_DATA.polyfit_tol_dir);

    addpath(SURFACE_DATA.polyfit_tol_dir);
    
end
    
    
%% Directory for polynomial fitting residual plots

resid_path = '/Outputs/Polyfit/ResidualPlots/';

SURFACE_DATA.resid_dir = [SURFACE_DATA.root_dir, resid_path];

if ~exist(SURFACE_DATA.resid_dir, 'dir')
    
    mkdir(SURFACE_DATA.resid_dir);

    addpath(SURFACE_DATA.resid_dir);
    
end

end