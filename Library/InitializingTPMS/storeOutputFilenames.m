function SURFACE_DATA = storeOutputFilenames(SURFACE_DATA)
% SURFACE_DATA = storeOutputFilenames(SURFACE_DATA)
%----------------------------------------------------------------------------
% storeOutputFilenames.m stores the filenames for all saved outputs in the
% SURFACE_DATA structure. The filenames are for all raw data and plots.
%
% Inputs:    SURFACE_DATA           struct
%
% Outputs:   SURFACE_DATA           struct
%
% Called by: setupSurfaceDataStructure.m
%----------------------------------------------------------------------------

%% Raw data filenames for text files

SURFACE_DATA.filenames.raw_data_volume = [SURFACE_DATA.alias, 'RawVolumeData.txt'];

SURFACE_DATA.filenames.raw_data_surface_area = [SURFACE_DATA.alias, 'RawSurfaceAreaData.txt'];

SURFACE_DATA.filenames.raw_data_strut_thickness = [SURFACE_DATA.alias, 'RawStrutThicknessData.txt'];

SURFACE_DATA.filenames.raw_data_double_surface_thickness = [SURFACE_DATA.alias, 'RawDoubleSurfaceThicknessData.txt'];


%% Output plot filenames for figs and pdfs

% figs

SURFACE_DATA.filenames.contour_plot_volume_fig = [SURFACE_DATA.alias, 'ContourVolume.fig'];

SURFACE_DATA.filenames.strut_plot_volume_fig = [SURFACE_DATA.alias, 'StrutVolume.fig'];

SURFACE_DATA.filenames.contour_plot_surface_area_fig = [SURFACE_DATA.alias, 'ContourSurfaceArea.fig'];

SURFACE_DATA.filenames.strut_plot_surface_area_fig = [SURFACE_DATA.alias, 'StrutSurfaceArea.fig'];

SURFACE_DATA.filenames.contour_plot_min_thickness_fig = [SURFACE_DATA.alias, 'ContourMinThickness.fig'];

SURFACE_DATA.filenames.strut_plot_min_thickness_fig = [SURFACE_DATA.alias, 'StrutMinThickness.fig'];


% pdfs

SURFACE_DATA.filenames.contour_plot_volume_pdf = [SURFACE_DATA.alias, 'ContourVolume'];

SURFACE_DATA.filenames.strut_plot_volume_pdf = [SURFACE_DATA.alias, 'StrutVolume'];

SURFACE_DATA.filenames.contour_plot_surface_area_pdf = [SURFACE_DATA.alias, 'ContourSurfaceArea'];

SURFACE_DATA.filenames.strut_plot_surface_area_pdf = [SURFACE_DATA.alias, 'StrutSurfaceArea'];

SURFACE_DATA.filenames.contour_plot_thickness_pdf = [SURFACE_DATA.alias, 'ContourThickness'];

SURFACE_DATA.filenames.strut_plot_thickness_pdf = [SURFACE_DATA.alias, 'StrutThickness'];

SURFACE_DATA.filenames.contour_plot_min_thickness_pdf = [SURFACE_DATA.alias, 'ContourMinThickness'];

SURFACE_DATA.filenames.strut_plot_min_thickness_pdf = [SURFACE_DATA.alias, 'StrutMinThickness'];


%% Output polynomial fitting coefficients filenames

SURFACE_DATA.filenames.coeff_volume_single = [SURFACE_DATA.alias, 'CoeffVolumeSingle.txt'];

SURFACE_DATA.filenames.coeff_volume_double = [SURFACE_DATA.alias, 'CoeffVolumeDouble.txt'];

SURFACE_DATA.filenames.coeff_surface_area_single = [SURFACE_DATA.alias, 'CoeffSurfaceAreaSingle.txt'];

SURFACE_DATA.filenames.coeff_surface_area_double = [SURFACE_DATA.alias, 'CoeffSurfaceAreaDouble.txt'];

SURFACE_DATA.filenames.coeff_thickness_single = [SURFACE_DATA.alias, 'CoeffThicknessSingle.txt'];

SURFACE_DATA.filenames.coeff_thickness_double = [SURFACE_DATA.alias, 'CoeffThicknessDouble.txt'];


%% Output polynomial fitting tolerance log filenames

SURFACE_DATA.filenames.tolerance_1D_log_filename = 'Polyfit1DToleranceLog.txt';

SURFACE_DATA.filenames.tolerance_2D_log_filename = 'Polyfit2DToleranceLog.txt';

end