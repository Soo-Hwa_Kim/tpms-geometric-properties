function varargout = lookupTPMSHandleAndOffsets(varargin)
% varargout = lookupTPMSHandleAndOffsets(varargin)
%----------------------------------------------------------------------------
% lookupTPMSHandleAndOffsets.m stores the implicit function handle 
% (fhandle), gradient function handle (ghandle), and the predetermined 
% offset range where the lattice is valid based on the alias. The offset
% range was determined a priori by visually looking at the surface struture
% at different offsets and seeing where the lattice is valid as well as
% when Newton-Raphson doesn't fail at correcting vertices
% (correctVertices.m)
%
% Inputs:    SURFACE_DATA           struct
%            OR
%            alias                  string
%            OR
%            (no input)             in this case fhandle and radius 
%                                   (offsets) for a sphere will be the 
%                                   output
%
% Outputs:   SURFACE_DATA           struct
%            OR
%            varargout              cell - {fhandle (handle), ghandle (handle)}
%
% Called by: prepareTPMSData.m
%----------------------------------------------------------------------------

%% Offset delta

offset_delta = 0.01;


%% Initialize flag for SURFACE_DATA structure

surface_data_flag = false;


%% Parse inputs

if isstruct(varargin{1})
    
    surface_data_flag = true;
    
    SURFACE_DATA = varargin{1};
    
    alias = SURFACE_DATA.alias;
    
    
else
    
    alias = varargin{1};
    
    
end


%% Look up fhandle, ghandle, and offsets

if contains(alias, 'rimitive') %Schwarz Primitive
    
    fhandle = @primitiveFun;
    
    ghandle = @gradientPrimitiveFun;
    
    offsets = -0.99 : offset_delta : 0.99;
    
    full_offsets = -2.99 : offset_delta : 2.99;
    
    
elseif contains(alias, 'yroid') %Schoen Gyroid
    
    fhandle = @gyroidFun;
    
    ghandle = @gradientGyroidFun;
    
    offsets = -1.35 : offset_delta : 1.35;
    
    full_offsets = -1.49 : offset_delta : 1.49;
    
    
elseif contains(alias, 'eovius') %Neovius
    
    fhandle = @neoviusFun;
    
    ghandle = @gradientNeoviusFun;
    
    offsets = -0.63 : offset_delta : 0.63;
    
    full_offsets = -12.99 : offset_delta : 1.99;
    
    
elseif contains(alias, 'iamon') %Schwarz Diamond
    
    fhandle = @diamondFun;
    
    ghandle = @gradientDiamondFun;
    
    offsets = -0.87 : offset_delta : 0.87;
    
    full_offsets = -1.41 : offset_delta : 1.41;
    
    
elseif contains(alias, 'WP') %iWP
    
    fhandle = @iWPFun;
    
    ghandle = @gradientiWPFun;
    
    offsets = -2.98 : offset_delta : 2.6;
    
    full_offsets = -4.99 : offset_delta : 2.99;
    
    
else
    
    fhandle = @sphereFun;
    
    ghandle = @gradientSphereFun;
    
    offsets = 0.5;
    
    
end


%% Outputs

if surface_data_flag
    
    SURFACE_DATA.fhandle = fhandle;
    
    SURFACE_DATA.ghandle = ghandle;
    
    SURFACE_DATA.offset_list = offsets;
    
    SURFACE_DATA.full_offset_list = full_offsets;
    
    varargout{1} = SURFACE_DATA;
    
    
else
    
    varargout{1} = fhandle;
    
    varargout{2} = ghandle;
    
    varargout{3} = offsets;
    
    varargout{4} = full_offsets;
    
    
end


end