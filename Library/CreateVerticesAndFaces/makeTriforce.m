function [new_faces, new_vertices, new_vertices_only] = makeTriforce(old_vertices, old_faces)
% [new_faces, new_vertices, new_vertices_only] = makeTriforce(old_vertices, old_faces)
%----------------------------------------------------------------------------
% makeTriforce.m creates more vertices and faces from an input set of
% vertices and faces for a triangular mesh. This is done by creating new
% vertices from the midpoints of the sides of each triangle. Output 
% vertices have old & new vertices together
% (new)
%
% Inputs:    old_faces                  numeric - nx3 matrix
%            old_vertices               numeric - mx3 matrix
%
% Outputs:   new_faces                  numeric - px3 matrix
%            new_vertices               numeric - qx3 matrix
%            new_vertices_only          numeric - rx3 matrix
%
% Called by: convergeSurfaceArea.m, createNewCorrectedVertices.m
%----------------------------------------------------------------------------

%% Setup and ID tracking

% Keep track of all IDs for vertices

original_num_vertex_IDs = length(old_vertices);


% Setup

current_face_ID1 = old_faces(:, 1);
current_face_ID2 = old_faces(:, 2);
current_face_ID3 = old_faces(:, 3);

vertex1 = old_vertices(current_face_ID1, :);
vertex2 = old_vertices(current_face_ID2, :);
vertex3 = old_vertices(current_face_ID3, :);


%% Create vertices and faces

% Create new vertices from midpoints

new_vertex1 = 0.5 * (vertex1 + vertex2);
new_vertex2 = 0.5 * (vertex2 + vertex3);
new_vertex3 = 0.5 * (vertex1 + vertex3);


% Create new vertex IDs

new_vertex_ID1 = ((original_num_vertex_IDs + 1):(original_num_vertex_IDs + length(new_vertex1)))';
new_vertex_ID2 = ((new_vertex_ID1(end) + 1):(new_vertex_ID1(end) + length(new_vertex2)))';
new_vertex_ID3 = ((new_vertex_ID2(end) + 1):(new_vertex_ID2(end) + length(new_vertex3)))';


% Create new set of faces (essentially creating a triforce from original
% triangle face)

new_faces1 = [current_face_ID1 new_vertex_ID1 new_vertex_ID3];
new_faces2 = [new_vertex_ID1 new_vertex_ID2 new_vertex_ID3];
new_faces3 = [new_vertex_ID3 new_vertex_ID2 current_face_ID3];
new_faces4 = [current_face_ID2 new_vertex_ID2 new_vertex_ID1];


% Total faces and vertices created

total_faces = [new_faces1; new_faces2; new_faces3; new_faces4];

new_vertices_only = [new_vertex1; new_vertex2; new_vertex3];

total_vertices = [old_vertices; new_vertices_only];


%% Check for repeats before finishing

[new_faces, new_vertices, new_vertices_only] = correctFacesAndVertices(total_faces, total_vertices, new_vertices_only);


end


function [new_faces, new_vertices, new_vertices_only] = correctFacesAndVertices(total_faces, total_vertices, new_vertices_only)
% [new_faces, new_vertices, new_vertices_only] = correctFacesAndVertices(total_faces, total_vertices, new_vertices_only)
%
% This function finds and purges repeating vertices and faces
%----------------------------------------------------------------------------

%% Find and purge any repeats

original_row_idx = (1:length(total_vertices))';

[new_vertices, ~, unique_row_idx] = uniquetol(total_vertices, 'ByRows', true);

new_faces = substituteSpecificValues(total_faces, unique_row_idx, original_row_idx);

new_vertices_only = uniquetol(new_vertices_only, 'ByRows', true);


end


