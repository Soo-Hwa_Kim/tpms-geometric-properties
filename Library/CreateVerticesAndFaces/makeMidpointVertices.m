function new_vertices = makeMidpointVertices(old_vertices, old_faces)
% new_vertices = makeMidpointVertices(old_vertices, old_faces)
%----------------------------------------------------------------------------
% makeMidpointVertices.m creates more vertices from an input set of
% vertices and faces for a triangular mesh. This is done by creating new
% vertices from the midpoints of the sides of each triangle. Only new
% vertices are output and old vertices are NOT included.
%
% Inputs:    old_faces                  numeric - nx3 matrix
%            old_vertices               numeric - mx3 matrix
%
% Outputs:   new_vertices               numeric - px3 matrix
%
% Called by: 
%----------------------------------------------------------------------------

%% Setup

current_face_ID1 = old_faces(:, 1);
current_face_ID2 = old_faces(:, 2);
current_face_ID3 = old_faces(:, 3);

vertex1 = old_vertices(current_face_ID1, :);
vertex2 = old_vertices(current_face_ID2, :);
vertex3 = old_vertices(current_face_ID3, :);


%% Create vertices from midpoints

new_vertex1 = 0.5 * (vertex1 + vertex2);
new_vertex2 = 0.5 * (vertex2 + vertex3);
new_vertex3 = 0.5 * (vertex1 + vertex3);

new_vertices = [new_vertex1; new_vertex2; new_vertex3];


%% Purge repeats

new_vertices = uniquetol(new_vertices, 'ByRows', true);


end