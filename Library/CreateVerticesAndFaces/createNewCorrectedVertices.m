function [faces, total_vertices] = createNewCorrectedVertices(vertices, faces, fhandle, ghandle, offset)
% [faces, old_vertices, new_vertices] = createNewCorrectedVertices(old_vertices, faces, fhandle, ghandle, offset_list)
%----------------------------------------------------------------------------
% createNewCorrectedVertices.m creates new vertices and faces and then
% corrects them using Newton-Raphson method. The output old_vertices is the
% total vertices including new_vertices and input old_vertices. The output 
% new_vertices is only the new vertices created.
%
% Inputs:    old_vertices           numeric - nx3 initial vertices
%            faces                  numeric - mx3 initial faces
%            fhandle                handle - TPMS function handle
%            ghandle                handle - TPMS gradient function handle
%            offset                 numeric - singular
%
% Outputs:   faces                  numeric - px3 new faces
%            total_vertices         numeric - tx3 total vertices
%
% Called by: convergeThicknessStrut.m, convergeThicknessDoubleSurfaces.m
%----------------------------------------------------------------------------

%% Create new vertices

[faces, total_vertices, ~] = makeTriforce(vertices, faces);


%% Correct vertices

total_vertices = correctVertices(total_vertices, fhandle, ghandle, offset);


end