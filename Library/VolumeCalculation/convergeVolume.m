function volume = convergeVolume(fhandle, offset)
% volume = convergeVolume(fhandle, offset)
%----------------------------------------------------------------------------
% convergeVolume.m calculates volume converged to a tolerance. The volume
% is calculated using Monte Carlo integration. This is repeated with
% increasing number of Monte Carlo points until the volume value has
% converged to a tolerance. The "monte_carlo_magnitude" refers to
% 10 ^ (monte_carlo_magnitude) number of points.
%
% Inputs:    fhandle                handle - TPMS implicit function
%            offset                 numeric
%
% Outputs:   volume                 numeric
%
% Called by: generateVolumeData.m
%----------------------------------------------------------------------------

%% Loop through calculating volume until value has converged

% Set up and pre-allocate

iteration = 1;

volumes = zeros(100, 1);

monte_carlo_magnitude = 7;

test_results = false;


% Loop through until volume difference is at or below tolerance

while ~test_results
    
    
    % Calculate volume
    
    volumes(iteration) = volumeMonteCarlo(fhandle, offset, monte_carlo_magnitude);
    
    
    % Check for convergence
    
    if iteration > 2
        
        [test_results, ~] = testConvergence(volumes((iteration-2):iteration));

        if test_results
            
            break
            
        end
        
    end
    
    
    % If has not passed convergence test increase number of Monte Carlo
    % points
    
    monte_carlo_magnitude = monte_carlo_magnitude + 0.1;
    
    iteration = iteration + 1;
    
 
end


% Output last converged volume value

volume = volumes(iteration);


end