function volume = volumeMonteCarlo(fhandle, offset, monte_carlo_magnitude)
% volume = volumeMonteCarlo(fhandle, offset, monte_carlo_magnitude)
%----------------------------------------------------------------------------
% volumeMonteCarlo.m calculates volume using Monte Carlo integration.
% Randomized points in a bounded volume are evaluated using the TPMS 
% implicit function to determine if the are "inside" or "outside" the 
% function. This function declares that if the value is negative it is 
% "inside". The ratio of "inside" points to total is the volume. 
%
% Inputs:    fhandle                handle - TPMS implicit function
%            offset                 numeric
%            monte_carlo_magnitude  numeric
%
% Outputs:   volume                 numeric
%
% Called by: convergeVolume.m
%----------------------------------------------------------------------------

%% Generate randomized numbers

% Number of Monte Carlo points

monte_carlo_points = round(10 ^ (monte_carlo_magnitude));


% Random numbers generated within bounds of one unit cell for the number 
% of Monte Carlos. One unit cell is one period of the lattice

bound_min = -0.5;

randomized_points = rand(monte_carlo_points, 3) + bound_min;


%% Calculate volume 

% TPMS function values

monte_carlo_function = fhandle(randomized_points(:,1), randomized_points(:,2), ...
    randomized_points(:,3), offset);


% Obtain volume using ratio of points "inside" and "outside" surface

inside_idx = monte_carlo_function <= 0;

inside_points = sum(inside_idx);

volume = inside_points / monte_carlo_points;


end