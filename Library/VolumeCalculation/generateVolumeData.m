function generateVolumeData(SURFACE_DATA)
% generateVolumeData(SURFACE_DATA)
%----------------------------------------------------------------------------
% generateVolumeData.m is the overhead function for calculating volume for 
% each offset and saving the volume data into a text file. This function  
% is only called if raw data for volume does not exist in correct 
% directory.
%
% Inputs:    SURFACE_DATA           struct
%
% Outputs:   raw data               saved text file
%
% Called by: TPMS_Analysis.m
%----------------------------------------------------------------------------

%% Calculate volume for each offset

% Setup and pre-allocate

offset_list = SURFACE_DATA.offset_list;

volume_matrix = zeros(length(offset_list), 2);


% Loop through each offset and calculate volume

for n = 1:length(offset_list)
    
    offset = offset_list(n);
    
    volume = convergeVolume(SURFACE_DATA.fhandle, offset);
    
    volume_matrix(n, :) = [offset, volume];
    
    
end


%% Save raw data into text file

filename = SURFACE_DATA.filenames.raw_data_volume;

data_dir = fullfile([SURFACE_DATA.raw_data_dir, filename]);

dlmwrite(data_dir, volume_matrix);


end