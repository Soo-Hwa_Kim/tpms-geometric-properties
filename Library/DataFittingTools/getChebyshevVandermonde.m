function [V, norm_factor] = getChebyshevVandermonde(offsets, rank)
% V = getChebyshevVandermonde(offsets)
%----------------------------------------------------------------------------
% getChebyshevVandermonde.m calculates the polynomials for the first 12 terms of
% the first kind.
%
% Inputs:    offsets                    numeric
%
% Outputs:   V                          numeric - matrix with calculated
%                                       polynomial values
%
% Called by: polyFitRawData.m, polyFit2DRawData.m
%----------------------------------------------------------------------------

if nargin < 2
    rank = 42;
end

% Normalize offsets to range of [-1, 1]
norm_factor = 1/max(abs(offsets));
xnorm = norm_factor * offsets;

% Calculate the Vandermonde matrix
V = getChebyshevTerms2(xnorm, rank);


end