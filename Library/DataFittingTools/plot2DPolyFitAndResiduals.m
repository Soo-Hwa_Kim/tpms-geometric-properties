function plot2DPolyFitAndResiduals(x, y, z, p, tpms, geom_prop, dir)


if size(x,2) == 1
    % Generate meshgrid for contour plots
    offsets = unique(x);
    [X, Y] = meshgrid(offsets, offsets);
    Z = nan(size(X));
    P = nan(size(Y));
    for n = 1:length(x)
        ind1 = find(offsets == x(n));
        ind2 = find(offsets == y(n));
        Z(ind1,ind2) = z(n);
        P(ind1,ind2) = p(n);
    end
else
    offsets = unique(x);
    X = x;
    Y = y;
    Z = z;
    P = p;
end

% Calculate residuals and prepare plot titles
% R = (P-Z)./Z;
R = (P-Z);
logR = log10(abs(R));
max_logR = max(max(logR));
R95length = round((size(R,1) - 0.95*size(R,1)) / 2);
R95 = R(R95length+1:end-R95length, R95length+1:end-R95length);
max95_logR = max(max(log10(abs(R95))));
titleP = sprintf('Raw Data & Polyfit %s', [tpms, ' ', geom_prop]);
titleR = sprintf('Residuals, max: log10(%.2f), 95%% max: log10(%.2f)', max_logR, max95_logR);


% Plot
fig1 = figure;
subplot(211)
[Cz, hz] = contour(X, Y, Z, 40, 'ShowText', 'on', 'LineColor', 'r');grid on;hold on
[Cv, hv] = contour(X, Y, P, 40, 'ShowText', 'on', 'LineColor', 'k');
hz.LevelList = round(hz.LevelList, 3);
clabel(Cz, hz, 'FontSize', 7)
hv.LevelList = round(hv.LevelList, 3);
clabel(Cv, hv, 'FontSize', 7)
axv = gca;
axv.FontSize = 7;
axv.XTick = min(offsets):0.05:max(offsets);
axv.YTick = min(offsets):0.05:max(offsets);
xlabel('Isovalue 1')
ylabel('Isovalue 2')
title(titleP)
legend('Raw data','Polyfit')

subplot(212)
imagesc(offsets, offsets, R)
set(gca, 'ydir', 'normal')
colorbar
xlabel('Isovalue 1')
ylabel('Isovalue 2')
title(titleR)


% Save plot (as .fig and .pdf files)
filename = [tpms, geom_prop, '2D'];
filename_fig = [filename, '.fig'];
filename_pdf = [filename, '.pdf'];
savefig([dir, filename_fig]) %save as fig
set(fig1,'Units', 'Inches');
pos = get(fig1, 'Position');
set(fig1, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
print(fig1, [dir, filename_pdf], '-dpdf', '-r0') %save as pdf


end