function T_nx = chebyshevGeneralFormula(n)

syms x

if n == 0
    
    df = (1-x^2)^(n-0.5);
    
else
    
    for m = 1:n
        
        if m == 1
            
            df = diff((1-x^2)^(n-0.5));
            
        else
            
            df = diff(df);
            
        end
        
    end
    
end

T_nx = (-2^n * factorial(n)) / (factorial(2*n)) * sqrt(1 - x^2) * df;


end