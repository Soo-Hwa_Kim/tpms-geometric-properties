function [V, poly_coefficients, tolerance, lowest_degree] = getPolyCoefficients(V, geometric_property)
% [V, poly_coefficients] = getPolyCoefficients(V, geometric_property)
%----------------------------------------------------------------------------
% getPolyCoefficients.m calculates coefficients and if they are
% significant. The tolerance is predetermined and when determining if the
% coefficients are significant care is taken to ensure the significance
% isn't oscillating between even and odd values.
%
% Inputs:    V                          numeric
%            geometric_property         numeric
%
% Outputs:   V                          numeric
%            poly_coefficients          numeric
%
% Called by: polyFitRawData.m
%----------------------------------------------------------------------------

% Tolerance for significant coefficients -- Predetermined

tolerance = 1E-4;
tolerance2 = 5E-3;


% Calculate coefficients

poly_coefficients = QRDecomposition(V, geometric_property);


% Determine which coefficients are significant

below_tol = find(abs(poly_coefficients) < tolerance);


% Pick the lowest degree index that is not oscillating above/below the 
% tolerance

lowest_degree_idx = min(find(diff(below_tol) == 1));

lowest_degree = below_tol(lowest_degree_idx+1);

if isempty(lowest_degree)
    tolerance = tolerance2;
    below_tol = find(abs(poly_coefficients) < tolerance);
    
    
    % Pick the lowest degree index that is not oscillating above/below the
    % tolerance
    
    lowest_degree_idx = min(find(diff(below_tol) == 1));
    
    lowest_degree = below_tol(lowest_degree_idx+1);
    
    if isempty(lowest_degree)
        warning('1d polyfit not converging!')
        keyboard
    end
    
end


% Calculate polynomial coefficients with significant polynomial degree
% terms

if ~isempty(lowest_degree)
    
    V = V(:, 1:lowest_degree);
    
    poly_coefficients = QRDecomposition(V, geometric_property);
    
end


end