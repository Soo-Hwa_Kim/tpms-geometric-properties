function residuals = calculateResiduals(V, geometric_property, poly_coefficients)

residuals = geometric_property - V * poly_coefficients;


end