function plotPolyFitAndResiduals(x, y, p, tpms, geom_prop, dir)

% Calculate residuals and prepare plot titles
R = p-y;
logR = log10(abs(R));
max_logR = max(logR);
titleP = sprintf('Raw Data & Polyfit %s', [tpms, ' ', geom_prop]);
titleR = sprintf('Residuals Data, max: log10(%.2f)', max_logR);


% Plot
fig1 = figure;
subplot(2,1,1)
plot(x, y,'.');hold on
plot(x, p)
xlabel('Isovalue')
ylabel(geom_prop)
xlim([min(x) max(x)])
legend('Raw data', 'Polyfit')
title(titleP)

subplot(2,1,2)
plot(x, R, '.')
xlabel('Isovalue')
ylabel('Residuals')
xlim([min(x) max(x)])
title(titleR)


% Save plot (as .fig and .pdf files)
filename = [tpms, geom_prop,'1D'];
filename_fig = [filename, '.fig'];
filename_pdf = [filename, '.pdf'];
savefig([dir, filename_fig]) %save as fig
set(fig1,'Units', 'Inches');
pos = get(fig1, 'Position');
set(fig1, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
print(fig1, [dir, filename_pdf], '-dpdf', '-r0') %save as pdf


end