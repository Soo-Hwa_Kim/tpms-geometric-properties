function runPolyFitOnAllData

% close all


poly_degree = 11;

tpms_aliases = {'Diamond', 'Gyroid', 'iWP', 'Neovius', 'Primitive'};


data_types = {'DoubleSurfaceThickness', 'StrutThickness', 'SurfaceArea', 'Volume'};

polyfit_data = struct();

for m = 1:length(tpms_aliases)
    
    alias = tpms_aliases{m};
    
   for n = 1:length(data_types)
       
       data_type = data_types{n};
       
       name = [alias, 'Raw', data_type, 'Data'];
       
       fname = [name, '.txt'];
       
       plotname = [alias, data_type];
       
       data = dlmread(fname);
       
       if n == 1
           
           X = data(:, 1);
           
           Y = data(:, 2);
           
           geometric_property = data(:, 3);
           
           
           [poly_coeff_degree, polyfit_vals] = polyFit2DRawData(X, Y, geometric_property, poly_degree);
           
           plot2DPolyFitAndResiduals(X, Y, geometric_property, polyfit_vals, plotname);
           
           polyfit_data.(name) = poly_coeff_degree;
           
       elseif n == 2
           
           offsets = data(:, 1);
           
           geometric_property = data(:, 2);
           
           
           [poly_coeff_degree, polyfit_vals] = polyFitRawData(offsets, geometric_property, poly_degree);
           
           plotPolyFitAndResiduals(offsets, geometric_property, polyfit_vals, plotname)
           
           polyfit_data.(name) = poly_coeff_degree;
           
           
       else
           
           offsets = data(:, 1);
           
           geometric_property = data(:, 2);
           
           
           [poly_coeff_degree, polyfit_vals] = polyFitRawData(offsets, geometric_property, poly_degree);
           
           plotPolyFitAndResiduals(offsets, geometric_property, polyfit_vals, plotname);
           
           name_single = [name, 'Single'];
           
           polyfit_data.(name_single) = poly_coeff_degree;
           
           
           offset_combos = nchoosek(offsets, 2);
           
           geometric_property2d = zeros(length(offset_combos), 1);
           
           for p = 1:length(offset_combos)
              
               offset_idx1 = find(offsets == offset_combos(p, 1));
               
               offset_idx2 = find(offsets == offset_combos(p, 2));
               
               if n == 3
                   
                  geometric_property2d(p) = geometric_property(offset_idx1) + geometric_property(offset_idx2);
                   
               else
                   
                   geometric_property2d(p) = abs(geometric_property(offset_idx1) - geometric_property(offset_idx2));
                   
               end
               
               
           end
           
           X = offset_combos(:, 1);
           
           Y = offset_combos(:, 2);
           
           [poly_coeff_degree2d, polyfit_vals2d] = polyFit2DRawData(X, Y, geometric_property2d, poly_degree);
           
           name_double = [name, 'Double'];
           
           polyfit_data.(name_double) = poly_coeff_degree2d;

           plot2DPolyFitAndResiduals(X, Y, geometric_property2d, polyfit_vals2d, plotname);
           
       end
       
   end
    
    
end



end