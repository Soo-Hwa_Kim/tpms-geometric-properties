function c = QRDecomposition(V, geometric_property)
% c = QRDecomposition(V, geometric_property)
%----------------------------------------------------------------------------
% QRDecomposition.m solves for coefficients, i.e. c*V = geometric_property, 
% using a QR decomposition
%
% Inputs:    V                          numeric
%            geometric_property         numeric
%
% Outputs:   c                          numeric
%
% Called by: getPolyCoefficients.m, getPolyCoefficients2D.m
%----------------------------------------------------------------------------

% QR decomposition

[Q, R] = qr(V, 0);


% Solve for coefficients

orig_state = warning;

warning('off', 'all')

c = R \ (Q' * geometric_property);

warning(orig_state)


end