function cheby_poly_mat = build2dChebyPolyMat(degree)

X = getChebyshevTermCoeffs(degree);
Y = X;
degree_combos = permn(1:degree+1, 2);
valid_combos = degree_combos(sum((degree_combos-1), 2) <= degree, :) - 1;

cheby_poly_mat = zeros(length(valid_combos),sum(1:(degree+1)));

for n = 1:length(valid_combos)
    
   Xrow = X(valid_combos(n,1)+1, :);
   Yrow = Y(valid_combos(n,2)+1, :);
   
   [~, ix, Xvals] = find(Xrow);
   [~, iy, Yvals] = find(Yrow);
   
   [A,B] = meshgrid(ix, iy);
   c = cat(2, A' ,B');
   d = reshape(c, [], 2);
   
   for m = 1:size(d, 1)
       
       if valid_combos(n,1) == 0
           
           value = Yrow(d(m,2));
           col_idx = sum(1:d(m,2));
           
       elseif valid_combos(n,2) == 0
                      
           value = Xrow(d(m,1));
           col_idx = sum(1:d(m,1))-(d(m,1)-1);
              
       else
      
           value = Xrow(d(m,1)) * Yrow(d(m,2));
           col_idx = sum(1:(d(m,1)-1)+(d(m,2)-1)) + d(m,2);
           
       end
       
       cheby_poly_mat(n, col_idx) = value;
   
   end
   
end

end