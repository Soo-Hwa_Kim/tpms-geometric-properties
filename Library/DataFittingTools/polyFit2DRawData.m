function [polyfit_vals, bn, tolerance, lowest_degree] = polyFit2DRawData(X, Y, geometric_property, poly_degree)
% [polyfit_vals, poly_coefficients] = polyFit2DRawData(offsets, geometric_property, poly_degree)
%----------------------------------------------------------------------------
% polyFit2DRawData.m performs a Chebyshev polynomial fitting to TPMS
% geometric data. This is specifically for 2D double surface data. Outputs
% the fitted values and the coefficients.
%
% Inputs:    offsets                    numeric
%            geometric_property         numeric
%            poly_degree                numeric - single value, optional
%
% Outputs:   polyfit_vals               numeric
%            poly_coefficients          numeric
%
% Called by: setupContourPlotInputs.m, generateCustomPlots.m
%----------------------------------------------------------------------------

% Check inputs whether polynomial degree is specified

if nargin < 4
    
    poly_degree = 13;
    
end


% Calculate Vandermonde matrices. Ensure we only have combined degree of 11
% (or specified input)

[Vx, norm_factor] = getChebyshevVandermonde(X, poly_degree);

[Vy, ~] = getChebyshevVandermonde(Y, poly_degree);

poly_degree = size(Vx,2) - 1;

degree_combos = permn(1:poly_degree+1, 2);

valid_combos = degree_combos(sum((degree_combos-1), 2) <= poly_degree, :);


V = zeros(length(Vx), length(valid_combos));

for n = 1:length(valid_combos)
    
    V(:, n) = Vx(:, valid_combos(n, 1)) .* Vy(:, valid_combos(n, 2));
    
end


% Calculate coefficients

[V, poly_coefficients, sig_degree, tolerance, lowest_degree] = getPolyCoefficients2D(V, geometric_property, valid_combos);


% Calculate fitted values

polyfit_vals = V * poly_coefficients;


% Polyfit model -- un-normalising Chebyshev model and putting in the format
% of y = b_0 + b_1*x + b_2^2*x^2 + ...

cheby_poly_mat = build2dChebyPolyMat(max(sig_degree));
an = repmat(poly_coefficients,1,size(cheby_poly_mat,2));
norm_coeff = sum(cheby_poly_mat .* an);
bn = zeros(length(an),1);
degree_counter = 0;
for n = 1:length(norm_coeff)
    if sum(1:(degree_counter+1)) < n
        degree_counter = degree_counter + 1;
    end
    bn(n) = norm_coeff(n) * norm_factor^(degree_counter);
end

%% TEST whether un-normalising works! Remove later!
bn_counter = 0;
test = zeros(length(X),1);
for degn = 1:lowest_degree+1
   ypower = 0:degn-1;
   Ypower = Y.^(ypower);
   xpower = fliplr(ypower);
   Xpower = X.^(xpower);
   bn_idx = bn_counter+1:bn_counter+length(ypower);
   this_test = sum(Ypower .* Xpower .* repmat(bn(bn_idx)',length(Ypower),1), 2);
   test = test+this_test;
   
   bn_counter = bn_counter+length(ypower);
   
end

if ~isequal(round(test,5), round(polyfit_vals,5))
    warning('Un-normalising not done properly for 2D!')
else
    disp('Output bn is correct (2D)')
end


end