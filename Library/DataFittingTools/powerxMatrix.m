function power_x = powerxMatrix(x, terms)

if nargin < 2
    terms = 11;
end

power_x = ones(length(x), terms+1);

for n = 1:terms
    
   power_x(:,n+1) = x.^n;
    
end

end