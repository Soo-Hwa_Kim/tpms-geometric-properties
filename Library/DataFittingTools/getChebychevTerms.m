function chebychev_terms = getChebychevTerms(x)
% chebychev_terms = getChebychevTerms(x)
%----------------------------------------------------------------------------
% chebychev_terms.m calculates the polynomials for the first 12 terms of
% the first kind.
%
% Inputs:    x                          numeric - normalized values
%
% Outputs:   chebychev_terms            numeric - Vandermonde matrix with 
%                                       calculated polynomial values
%
% Called by: getChebychevVandermonde.m
%----------------------------------------------------------------------------

%Chebychev polynomials

T0 = ones(length(x), 1);

T1 = x;

T2 = 2*x.^2 - 1;

T3 = 4*x.^3 - 3*x;

T4 = 8*x.^4 - 8*x.^2 + 1;

T5 = 16*x.^5 - 20*x.^3 + 5*x;

T6 = 32*x.^6 - 48*x.^4 + 18*x.^2 - 1;

T7 = 64*x.^7 - 112*x.^5 + 56*x.^3 - 7*x;

T8 = 128*x.^8 - 256*x.^6 + 160*x.^4 - 32*x.^2 + 1;

T9 = 256*x.^9 - 576*x.^7 + 432*x.^5 - 120*x.^3 + 9*x;

T10 = 512*x.^10 - 1280*x.^8 + 1120*x.^6 - 400*x.^4 + 50*x.^2 - 1;

T11 = 1024*x.^11 - 2816*x.^9 + 2816*x.^7 - 1232*x.^5 + 220*x.^3 - 11*x;

% T12 = (2*x) .* T11 - T10;
% 
% T13 = (2*x) .* T12 - T11;
% 
% T14 = (2*x) .* T13 - T12;


% Output

chebychev_terms = [T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11];%, T12, T13, T14];


end