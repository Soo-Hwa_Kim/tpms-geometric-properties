function V = getChebyshevVandermonde(offsets)
% V = getChebyshevVandermonde(offsets)
%----------------------------------------------------------------------------
% getChebyshevVandermonde.m calculates the polynomials for the first 12 terms of
% the first kind.
%
% Inputs:    offsets                    numeric
%
% Outputs:   V                          numeric - matrix with calculated
%                                       polynomial values
%
% Called by: polyFitRawData.m, polyFit2DRawData.m
%----------------------------------------------------------------------------

% Normalize offsets to range of [-1, 1]

xnorm = normalize(offsets, 'range', [-1 1]);


% Calculate the Vandermonde matrix

V = getChebyshevTerms2(xnorm);


end