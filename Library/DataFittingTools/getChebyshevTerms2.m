function vandermonde = getChebyshevTerms2(x, rank)

chebyshev_poly_coeff = getChebyshevTermCoeffs(rank);
power_x = powerxMatrix(x,rank);

for n = 1:length(x)
    
chebyshev_terms = repmat(power_x(n,:),length(chebyshev_poly_coeff),1) .* chebyshev_poly_coeff;
vandermonde(n, :) = sum(chebyshev_terms, 2)';

end


end

