function [polyfit_vals, bn, tolerance, lowest_degree] = polyFitRawData(offsets, geometric_property, poly_degree)
% [polyfit_vals, poly_coefficients] = polyFitRawData(offsets, geometric_property, poly_degree)
%----------------------------------------------------------------------------
% polyFitRawData.m performs a Chebyshev polynomial fitting to TPMS
% geometric data. This is specifically for 1D single surface data. Outputs
% the fitted values and the coefficients.
%
% Inputs:    offsets                    numeric
%            geometric_property         numeric
%            poly_degree                numeric - single value, optional
%
% Outputs:   polyfit_vals               numeric
%            poly_coefficients          numeric
%
% Called by: setupAndGeneratePlots.m, generateCustomPlots.m
%----------------------------------------------------------------------------

% Check inputs whether polynomial degree is specified

% if nargin < 3
%     
%     poly_degree = 11;
%     
% end

% Calculate Vandermonde matrix

[V, norm_factor] = getChebyshevVandermonde(offsets);


% Calculate coefficients

[V, poly_coefficients, tolerance, lowest_degree] = getPolyCoefficients(V, geometric_property);


% Calculate fitted values

polyfit_vals = V * poly_coefficients;


% Polyfit model -- un-normalising Chebyshev model and putting in the format
% of y = b_0 + b_1*x + b_2^2*x^2 + ...

chebyshev_poly_coeff = getChebyshevTermCoeffs(length(poly_coefficients)-1);
an = repmat(poly_coefficients,1,length(poly_coefficients));
norm_coeff = sum(chebyshev_poly_coeff .* an);
bn = zeros(length(an),1);
for n = 1:length(norm_coeff)
    bn(n) = norm_coeff(n) * norm_factor^(n-1);
end

%% TEST whether un-normalising works! Remove later!
power_offset = powerxMatrix(offsets, length(bn)-1);
bns = repmat(bn',length(offsets),1);
test = sum(bns .* power_offset,2);
if ~isequal(round(test,3), round(polyfit_vals,3))
    warning('Un-normalising not done properly for 1D!')
else
    disp('Output bn is correct (1D)')
end


end