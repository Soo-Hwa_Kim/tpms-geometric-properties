function [V, poly_coefficients, sig_degree, tolerance, lowest_degree] = getPolyCoefficients2D(V, geometric_property, combos)
% [V, poly_coefficients] = getPolyCoefficients2D(V, geometric_property, combos)
%----------------------------------------------------------------------------
% getPolyCoefficients2D.m calculates coefficients for 2D data and if they 
% are significant. The tolerance is predetermined and when determining if 
% the coefficients are significant care is taken to ensure the significance
% isn't oscillating between even and odd values.
%
% Inputs:    V                          numeric
%            geometric_property         numeric
%            combos                     numeric
%
% Outputs:   V                          numeric
%            poly_coefficients          numeric
%            combos                     numeric
%
% Called by: polyFit2DRawData.m
%----------------------------------------------------------------------------

% Tolerance for significant coefficients -- Predetermined

tolerance = 1E-4;
tolerance2 = 5E-3;
tolerance3 = 1E-3;
tolerance4 = 5E-2;


% Calculate coefficients

degrees = sum(combos-1, 2);

poly_coefficients = QRDecomposition(V, geometric_property);


% Determine which coefficients are significant

try
    degrees_below_tol = determineBelowTol(degrees, poly_coefficients, tolerance);
    if numel(degrees_below_tol) < 2
        error('degrees below tol < 2')     
    end
catch
    try
        tolerance = tolerance2;
        degrees_below_tol = determineBelowTol(degrees, poly_coefficients, tolerance);
        if numel(degrees_below_tol) < 2
            error('degrees below tol < 2')
        end
    catch
        try
            tolerance = tolerance3;
            degrees_below_tol = determineBelowTol(degrees, poly_coefficients, tolerance);
            if numel(degrees_below_tol) < 2
                error('degrees below tol < 2')
            end
        catch
            tolerance = tolerance4;
            degrees_below_tol = determineBelowTol(degrees, poly_coefficients, tolerance);
        end
    end
end
% if numel(degrees_below_tol) < 2
%    disp('Did not meet convergence!!')
%    keyboard
%     return
%     
% 
% end

not_osc_idxs = find(diff(degrees_below_tol) == 1);
try
lowest_degree = degrees_below_tol(min(not_osc_idxs)+1);
catch
    keyboard
end

% Calculate polynomial coefficients with significant polynomial degree
% terms

if ~isempty(lowest_degree)
    
    sig_rows = degrees <= lowest_degree;
    
    V = V(:, sig_rows);
    
    poly_coefficients = QRDecomposition(V, geometric_property);
    
    sig_degree = degrees(sig_rows);
    
    
end


end

function degrees_below_tol = determineBelowTol(degrees, poly_coefficients, tolerance)

below_tol = degrees(abs(poly_coefficients) < tolerance);

[GC, GR] = groupcounts(below_tol);

degrees_below_tol = GR(find(((GR+1) - GC) == 0));

end