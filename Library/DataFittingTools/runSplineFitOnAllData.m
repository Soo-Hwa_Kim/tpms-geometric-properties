function runPolyFitOnAllData


% tpms_aliases = {'Diamond', 'Gyroid', 'iWP', 'Neovius', 'Primitive'};

% data_types = {'DoubleSurfaceThickness', 'StrutThickness', 'SurfaceArea', 'Volume'};

tpms_aliases = {'Diamond', 'Gyroid', 'iWP', 'Neovius', 'Primitive'};

data_types = {'StrutThickness', 'SurfaceArea', 'Volume'};



polyfit_data = struct();

for m = 1:length(tpms_aliases)
    
    alias = tpms_aliases{m};
    
    for n = 1:length(data_types)
        
        data_type = data_types{n};
        
        name = [alias, 'Raw', data_type, 'Data'];
        
        fname = [name, '.txt'];
        
        plotname = [alias, data_type];
        
        data = dlmread(fname);
        
        
        
        offsets = data(:, 1);
        
        geometric_property = data(:, 2);
        
        
        sample_idxs = floor(linspace(1, length(offsets), (2*length(offsets)/10)));
        
        sample_offsets = offsets(sample_idxs);
        
        sample_geometric_property = geometric_property(sample_idxs);
        
        spline_geometric_property = spline(sample_offsets, sample_geometric_property, offsets);
        
        
        plotPolyFitAndResiduals(offsets, geometric_property, spline_geometric_property, plotname)
        
        
        
    end
    
    
end



end