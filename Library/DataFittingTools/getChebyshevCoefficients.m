function T = getChebyshevTermCoefficients(rank)

if nargin < 1
    rank = 11;
end

% Pre-allocate
T = zeros(rank+1);

% Chebyshev term coefficients
for n = 1:rank+1
    
   if n == 1 || n == 2
       T(n,n) = 1;
   else
       term1 = 2*fliplr(nonzeros(T(n-1, :))');
       term2 = -fliplr(nonzeros(T(n-2, :))');
       i = (-1)^n;
       if i > 0
           combi_terms = term1(2:end) + term2;
           term2end = 0;
       else
           combi_terms = term1(2:end) + term2(1:end-1);
           term2end = term2(end);
       end
       all_terms = fliplr([term1(1) combi_terms term2end]);
       all_terms(2,:) = 0;
       Tn = all_terms(:);
       if i > 0
           T(n,1:length(Tn)-2) = Tn(2:end-1);
       else
           T(n,1:length(Tn)-1) = Tn(1:end-1);
       end
       
   end
    
end


end