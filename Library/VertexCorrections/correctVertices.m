function vertices = correctVertices(vertices, fhandle, ghandle, offset)
% vertices = correctVertices(vertices, fhandle, ghandle, offset)
%----------------------------------------------------------------------------
% correctVertices.m corrects input vertices (generated from MATLAB
% isosurface built-in function) to more accurate values on the surface
% using Newton-Raphson method. It loops through Newton-Raphson method 7
% times though this is generous since it should only take 2-3 loops.
%
% Inputs:    vertices               numeric - nx3 matrix
%            fhandle                handle - TPMS implicit function
%            ghandle                handle - TPMS gradient function
%            offset                 numeric
%
% Outputs:   vertices               numeric - nx3 matrix (corrected values)
%
% Called by: getInitialFacesAndVertices.m, convergeSurfaceArea.m
%----------------------------------------------------------------------------

%% Loop through Newton-Raphson method to correct vertices

number_of_iterations = 7;


for n = 1:number_of_iterations

    
    fvertices = fhandle(vertices(:,1), vertices(:,2), vertices(:,3), offset);
    
    gvertices = ghandle(vertices(:,1), vertices(:,2), vertices(:,3));
    
    vertices = newtonRaphson3DCorrection(fvertices, gvertices, vertices);
    
    
end


end