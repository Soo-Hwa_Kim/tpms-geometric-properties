function corrected_vertices = newtonRaphson3DCorrection(function_values, gradient_values, original_vertices)
% corrected_vertices = newtonRaphson3DCorrection(function_values, gradient_values, original_vertices)
%----------------------------------------------------------------------------
% newtonRaphson3DCorrection.m corrects input vertices (generated from MATLAB
% isosurface built-in function) to more accurate values on the surface
% using Newton-Raphson method. This is one iteration of Newton-Raphson
% method.
%
% Inputs:    original_vertices              numeric - nx3 matrix
%            function_values                numeric - nx3 matrix
%            gradient_values                numeric - nx3 matrix
%
% Outputs:   corrected_vertices             numeric - nx3 matrix (corrected values)
%
% Called by: correctVertices.m
%----------------------------------------------------------------------------

%% Newton-Raphson method

correction_magnitude = function_values  ./ (calculateVectorMagnitude(gradient_values) .^ 2);

corrected_vertices = original_vertices - (repmat(correction_magnitude, 1, 3) .* gradient_values);


end