function [test_results, converged_idx] = testConvergence(geometric_data)
% [test_results, converged_idx] = testConvergence(thickness, previous_thickness)
%----------------------------------------------------------------------------
% testConvergence.m tests whether the geometric data has converged within 
% the set tolerance for three most recent iterations. The convergence test
% is relative to the prior geometric data values.
%
% Inputs:    geometric_data         numeric - nx3
%
% Outputs:   test_results           logical - nx1 pass or not for each
%                                   thickness value
%            converged_idx          numeric - indices of thickness values
%                                   within tolerance
%
% Called by: convergeThicknessStrut.m, convergeThicknessDoubleSurfaces.m
%----------------------------------------------------------------------------

%% Check input matrix dimensions

% We want an nx3 matrix

if size(geometric_data, 2) ~= 3
    
    geometric_data = geometric_data';

end


%% Check for convergence

% Set tolerance

tolerance = 0.01;
% tolerance = 5E-4;


% Test if last iteration is within tolerance of the previous two

test1 = abs((geometric_data(:,3) ./ geometric_data(:,1)) - 1) < tolerance;

test2 = abs((geometric_data(:,3) ./ geometric_data(:,2)) - 1) < tolerance;


% Results if both tests are true and respective indices

test_results = test1 & test2;

converged_idx = find(test_results);


end