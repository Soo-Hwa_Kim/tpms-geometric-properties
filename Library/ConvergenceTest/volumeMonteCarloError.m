function volumeMonteCarloError

alias = 'Primitive';

SURFACE_DATA = setupSurfaceDataStructure(alias);


% Prepare TPMS data within structure

SURFACE_DATA = prepareTPMSData(SURFACE_DATA);


assessVolumeError(SURFACE_DATA);



end