function assessVolumeError(SURFACE_DATA)

offset_list = SURFACE_DATA.offset_list;

convergence_tolerance = 0.01;

trials = 20;

volume_matrix = zeros(length(offset_list), trials);

monte_carlo_matrix = zeros(length(offset_list), trials);

for m = 1:trials
    
    % Loop through each offset and calculate volume
    
    for n = 1:length(offset_list)
        
        offset = offset_list(n);
        
        [volume, monte_carlo] = convergeVolumeErrorTest(SURFACE_DATA.fhandle, offset);
        
        volume_matrix(n, m) = volume;
        
        monte_carlo_matrix(n, m) = monte_carlo;
        
        
    end
    
end

mean_volume = sum(volume_matrix, 2) / trials;

std_volume = sqrt((1/(trials-1)) * sum((volume_matrix - repmat(mean_volume, 1, trials)).^2, 2));

num_std = convergence_tolerance / max(std_volume);

statement = sprintf('Tolerance is within %d standard deviations', num_std);

fprintf(statement)


end