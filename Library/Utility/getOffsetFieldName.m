function offset_field_name = getOffsetFieldName(offset)
% offset_field_name = getOffsetFieldName(offset)
%----------------------------------------------------------------------------
% getOffsetFieldName.m gives the name of the structure field name for a
% specific offset
%
% Inputs:    offset                 numeric - singular
%
% Outputs:   offset_field_name      string
%
% Called by: getVerticesFromStepEstimate.m,
%            calculateThicknessForOffsetCombinations.m
%
%----------------------------------------------------------------------------

%% Obtain structure field name

if sign(offset) == -1
    
    offset_field_name = ['n', num2str(abs(offset) * 100)];
    
else
    
    offset_field_name = ['p', num2str(offset * 100)];
    
end


end