function aliases = parseInputs(varargin)
% aliases = parseInputs(varargin)
%----------------------------------------------------------------------------
% parseInputs.m checks the inputs to TPMS_Analysis.m before performing 
% main analysis. If user has put inputs in incorrectly or nothing at all,
% the default is to run TPMS_Analysis.m for all five TPMS.
%
% Inputs:    varargin               cell - containing the char array(s) 
%                                   for TPMS alias(es)
%
% Outputs:   aliases                cell - containing the char array(s) 
%                                   for TPMS alias(es)
%
% Called by: TPMS_Analysis.m
%----------------------------------------------------------------------------

%% Check inputs

% Main program varargin (not current function varargin)

main_arg_in = varargin{:};


% Is there an input?

if isempty(main_arg_in)
    
    warning('No TPMS were specified. All five TPMS will be analyzed by default.')
    
    aliases = {'Primitive', 'Gyroid', 'Diamond', 'Neovius', 'iWP'};
    
    
else
    
    % Is it in the correct format?
    
    if ~ischar(main_arg_in{1})
        
        warning('Input is not char format and cannot be interpreted. All five TPMS will be analyzed by default.')
        
        aliases = {'Primitive', 'Gyroid', 'Diamond', 'Neovius', 'iWP'};
        
        
    else % user input is correctly formatted and will run as is
        
        aliases = main_arg_in;
        
        
    end
    
    
end

% Delete any prior polyfit tolerance logs
polyfit_dir = fullfile([pwd,'\Outputs\Polyfit\ToleranceLog\']);
if exist(polyfit_dir, 'dir')
    delete Outputs\Polyfit\ToleranceLog\*.txt
end

end