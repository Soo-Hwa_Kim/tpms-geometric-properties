function magnitude = calculateVectorMagnitude(vector)
% magnitude = calculateVectorMagnitude(vector)
%----------------------------------------------------------------------------
% calculateVectorMagnitude.m calculates the magnitude of a 3D vector
%
% Inputs:    vector                numeric - nx3 matrix
%
% Outputs:   magnitude             numeric - nx1 vector
%
% Called by: newtonRaphson3DCorrection.m
%----------------------------------------------------------------------------

%% Calculate magnitude

if size(vector,2) == 2
    vector = [vector, zeros(size(vector,1),1)];
end

magnitude = sqrt(vector(:, 1) .^ 2 + vector(:, 2) .^ 2 + vector(:, 3) .^ 2);


end