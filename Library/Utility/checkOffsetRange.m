function raw_data = checkOffsetRange(raw_data)


if size(raw_data, 2) == 2
    
   if abs(min(raw_data(:, 1))) ~= abs(max(raw_data(:, 1)))
        
        limit = min(abs(min(raw_data(:, 1))), abs(max(raw_data(:, 1))));
        
        raw_data = raw_data(find(raw_data(:, 1) == -limit) : find(raw_data(:, 1) == limit), :);
        
   end 
    
else
    
    
    if max(abs(raw_data(:, 1))) ~= max(abs(raw_data(:, 2)))
        
        limitcol1 = min(abs(min(raw_data(:, 1))), abs(max(raw_data(:, 1))));
        limitcol2 = min(abs(min(raw_data(:, 2))), abs(max(raw_data(:, 2))));
        limit = max(limitcol1, limitcol2);
        col1rows = intersect(find(raw_data(:,1)>-limit), find(raw_data(:,1)<limit));
        col2rows = intersect(find(raw_data(:,2)>-limit), find(raw_data(:,2)<limit));
        valid_rows = intersect(col1rows, col2rows);
        raw_data = raw_data(valid_rows, :);
        
    end
    
    
end


end