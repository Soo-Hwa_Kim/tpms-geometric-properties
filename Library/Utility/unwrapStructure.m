function var = unwrapStructure(var)
  
field = fieldnames(var);
var = var.(field{1});  
  
end