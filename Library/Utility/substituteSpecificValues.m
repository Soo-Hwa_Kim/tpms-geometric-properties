function matrix = substituteSpecificValues(matrix, newcode, oldcode)
% matrix = substituteSpecificValues(matrix, newcode, oldcode)
%----------------------------------------------------------------------------
% substituteSpecificValues.m replaces specific values (oldcode) in a matrix
% with new values (newcode). newcode and oldcode must be the same size
%
% Inputs:    matrix                     numeric
%            newcode                    numeric
%            oldcode                    numeric
%
% Outputs:   matrix                     numeric
%
% Called by: makeTriforce.m
%----------------------------------------------------------------------------

%% Find and replace old values with new values in matrix

assert(numel(newcode) == numel(oldcode), 'newcode and oldecode must have the same number of elements');

[toreplace, bywhat] = ismember(matrix, oldcode);

matrix(toreplace) = newcode(bywhat(toreplace));


end