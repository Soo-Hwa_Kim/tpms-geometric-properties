function [xyz] = rbe_to_xyz(rbe)
%--------------------------------------------------------------------------
% Transforms RBE (spherical) to ENU (cartesian) using a matrix of x,y,z
% values (matrix with row 1 as r, row 2 as b, row 3 as e)
%
% Input: rbe         numerical - 3xn matrix, row 1 as range, row 2 as 
%                   bearing, and row 3 as elevation
%
% Output: xyz        numerical - 3xn matrix, row 1 as x, row 2 as y, row 3
%                   as z
%--------------------------------------------------------------------------
rbe = rbe';
rbex = rbe(1,:) .* cos(rbe(3,:)) .* sin(rbe(2,:));

rbey = rbe(1,:) .* cos(rbe(3,:)) .* cos(rbe(2,:));

rbez = rbe(1,:) .* sin(rbe(3,:));


xyz = [rbex; rbey; rbez]';

end