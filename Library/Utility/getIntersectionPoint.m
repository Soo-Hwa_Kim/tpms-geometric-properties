function intersection_points = getIntersectionPoint(p1, p2, q1, q2)

warning('off', 'last')



b = (q1 - p1)';
b = b(:);

A1 = (p2 - p1)';
A1 = A1(:);
A2 = (q1 - q2)';
A2 = A2(:);
A = [A1, A2];

dim = size(A,1) / 2;

cellA = mat2cell(A, (2 * ones(1, dim)));




A = blkdiag(cellA{:});





intersection_points = A \ b;

intersection_points = reshape(intersection_points, [], 2);

end