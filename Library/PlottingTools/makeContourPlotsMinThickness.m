function makeContourPlotsMinThickness(data, alias)

combos = data(:, 1:2);
min_thicknesses = data(:, 3);

offsets = unique(combos);

[x,y] = meshgrid(offsets, offsets);

zmin = zeros(size(x));

for n = 1:size(combos, 1)
    
    ind1 = find(offsets == combos(n,1));
    ind2 = find(offsets == combos(n,2));
    zmin(ind1,ind2) = min_thicknesses(n);
    
end

zmin(zmin == 0) = nan;


figure;
[Cv, hv] = contour(x, y, zmin, 40, 'ShowText', 'on', 'LineColor', 'k');grid on
hv.LevelList = round(hv.LevelList, 3);
clabel(Cv, hv, 'FontSize', 7)
axv = gca;
axv.FontSize = 7;
axv.XTick = min(offsets):0.05:max(offsets);
axv.YTick = min(offsets):0.05:max(offsets);
xlabel('Offset 1')
ylabel('Offset 2')
if nargin == 1
    title('Min Thicknesses')
else
    title([alias,' Min Thicknesses'])
end


end