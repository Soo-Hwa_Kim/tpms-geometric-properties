function generatePlots(SURFACE_DATA, contour_plot_inputs, raw_data, geometric_property)
% generatePlots(SURFACE_DATA, contour_plot_inputs, raw_data, geometric_property)
%----------------------------------------------------------------------------
% generatePlots.m creates plots for volume, surface area, and minimum
% thicknesses for solid struts and double surfaces. The visualization is
% turned off and the plots are saved into organized output directories.
%
% Inputs:    SURFACE_DATA           struct
%            contour_plot_inputs    cell
%            raw_data               numeric
%            geometric_property     string
%
% Outputs:   plots                  saved pdf and fig files
%
% Called by: setupAndGeneratePlots.m
%----------------------------------------------------------------------------

%% Turn off visualization (too many plots will come up otherwise)

set(0,'DefaultFigureVisible','off');


%% Set up

% Offset list for plot axes

offset_list = SURFACE_DATA.offset_list;


%% Create plots

if contains(geometric_property, 'Thickness')
    
    axis_label = 'Minimum Thickness';
    
    if contains(geometric_property, 'Strut')
        
        % Grab filenames for plots
        
        strut_plot_filename_fig = SURFACE_DATA.filenames.strut_plot_min_thickness_fig;
        
        strut_plot_filename_pdf = SURFACE_DATA.filenames.strut_plot_min_thickness_pdf;
        
        
        % Create/save minimum thickness plot for struts
        
        fig1 = figure;
        plot(raw_data(:,1), raw_data(:,2), 'k', 'LineWidth', 2)
        xlim([offset_list(1) offset_list(end)])
        ylim([min(raw_data(:,2)) max(raw_data(:,2))])
        axv = gca;
        axv.FontSize = 7;
        axv.XTick = offset_list(1):0.05:offset_list(end);
        xlabel('Isovalue', 'interpreter', 'latex', 'FontWeight', 'bold')
        ylabel(axis_label, 'interpreter', 'latex', 'FontWeight', 'bold')
        
        savefig([SURFACE_DATA.plot_fig_dir, strut_plot_filename_fig]) %save as fig
        set(fig1,'Units', 'Inches');
        pos = get(fig1, 'Position');
        set(fig1, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
        print(fig1, [SURFACE_DATA.plot_pdf_dir, strut_plot_filename_pdf], '-dpdf', '-r0') %save as pdf
        
        
    else % Double surfaces
        
        % Grab filenames for plots
        
        contour_plot_filename_fig = SURFACE_DATA.filenames.contour_plot_min_thickness_fig;
        
        contour_plot_filename_pdf = SURFACE_DATA.filenames.contour_plot_min_thickness_pdf;
        
        
        % Create/save minimum thickness contour plot for double surfaces
        
        fig1 = figure;
        [Cmin, hmin] = contour(contour_plot_inputs{:}, 40, 'ShowText', 'on', 'LineColor', 'k');grid on
        xlim([offset_list(1) offset_list(end)])
        ylim([offset_list(1) offset_list(end)])
        hmin.LevelList = round(hmin.LevelList, 3);
        clabel(Cmin, hmin, 'FontSize', 7)
        axv = gca;
        axv.FontSize = 7;
        axv.XTick = round(linspace(offset_list(1), offset_list(end), 10), 2);
        axv.YTick = round(linspace(offset_list(1), offset_list(end), 10), 2);
        xlabel('Isovalue 1', 'interpreter', 'latex', 'FontWeight', 'bold')
        ylabel('Isovalue 2', 'interpreter', 'latex', 'FontWeight', 'bold')
        
        savefig([SURFACE_DATA.plot_fig_dir, contour_plot_filename_fig]) %save as fig
        set(fig1,'Units','Inches');
        pos = get(fig1,'Position');
        set(fig1,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
        print(fig1, [SURFACE_DATA.plot_pdf_dir, contour_plot_filename_pdf], '-dpdf', '-r0') %save as pdf
        
    end
    
    
else
    
    % Grab filenames for plots
    
    if contains(geometric_property, 'Volume')
        
        contour_plot_filename_fig = SURFACE_DATA.filenames.contour_plot_volume_fig;
        
        contour_plot_filename_pdf = SURFACE_DATA.filenames.contour_plot_volume_pdf;
        
        strut_plot_filename_fig = SURFACE_DATA.filenames.strut_plot_volume_fig;
        
        strut_plot_filename_pdf = SURFACE_DATA.filenames.strut_plot_volume_pdf;
        
    else
        
        contour_plot_filename_fig = SURFACE_DATA.filenames.contour_plot_surface_area_fig;
        
        contour_plot_filename_pdf = SURFACE_DATA.filenames.contour_plot_surface_area_pdf;
        
        strut_plot_filename_fig = SURFACE_DATA.filenames.strut_plot_surface_area_fig;
        
        strut_plot_filename_pdf = SURFACE_DATA.filenames.strut_plot_surface_area_pdf;
        
    end
    
    
    % Create/save contour plot
    
    fig1 = figure;
    [Cv, hv] = contour(contour_plot_inputs{:}, 40, 'ShowText', 'on', 'LineColor', 'k');grid on
    xlim([offset_list(1) offset_list(end)])
    ylim([offset_list(1) offset_list(end)])
    hv.LevelList = round(hv.LevelList, 3);
    clabel(Cv, hv, 'FontSize', 7)
    axv = gca;
    axv.FontSize = 7;
    axv.XTick = round(linspace(offset_list(1), offset_list(end), 10), 2);
    axv.YTick = round(linspace(offset_list(1), offset_list(end), 10), 2);
    xlabel('Isovalue 1', 'interpreter', 'latex', 'FontWeight', 'bold')
    ylabel('Isovalue 2', 'interpreter', 'latex', 'FontWeight', 'bold')
    
    savefig([SURFACE_DATA.plot_fig_dir, contour_plot_filename_fig]) %save as fig
    set(fig1,'Units', 'Inches');
    pos = get(fig1, 'Position');
    set(fig1, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
    print(fig1, [SURFACE_DATA.plot_pdf_dir, contour_plot_filename_pdf], '-dpdf', '-r0') %save as pdf
    
    
    % Create/save strut plot
    
    fig2 = figure;
    plot(raw_data(:,1), raw_data(:,2), 'k', 'LineWidth', 2)
    xlim([offset_list(1) offset_list(end)])
    ylim([min(raw_data(:,2)) max(raw_data(:,2))])
    axv = gca;
    axv.FontSize = 7;
    axv.XTick = offset_list(1):0.05:offset_list(end);
    xlabel('Isovalue', 'interpreter', 'latex', 'FontWeight', 'bold')
    ylabel(geometric_property, 'interpreter', 'latex', 'FontWeight', 'bold')
    
    savefig([SURFACE_DATA.plot_fig_dir, strut_plot_filename_fig]) %save as fig
    set(fig2,'Units', 'Inches');
    pos = get(fig2, 'Position');
    set(fig2, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
    print(fig2, [SURFACE_DATA.plot_pdf_dir, strut_plot_filename_pdf], '-dpdf', '-r0') %save as pdf
    
    
end


end