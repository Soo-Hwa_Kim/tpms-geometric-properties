% Script to plot a single unit cell
% clear all
% clf
eps = 1e-4; % A very small number




%% Define the meshgrid to evaluate all functions on
xLim = 0.5;
yLim = 0.5;
zLim = 0.5;



xStep = 300;
yStep = 300;
zStep = 300;



[X, Y, Z] = meshgrid(linspace(-xLim - eps, xLim + eps, xStep), linspace(-yLim - eps, yLim + eps, yStep), linspace(-zLim - eps, zLim + eps, zStep));


%% Define the objects to be manipulated and plotted
%offsets
% t1 = 0.4;
% t2 = -t1;
% t3 = 0;
incr = 5;
ts_ds = linspace(1,0.1,incr);
% ts_ds = linspace(0.05,1.35,5);
ts_ss = linspace(-1.35,0,incr);

%unit cube length
L = 1;

%frequency
f = 2 * pi / L;


% 
% % First Gyroid surface
% obj1 = gyroid(X, Y, Z, t1, f);
% obj2 = gyroid(X, Y, Z, t2, f);
% obj5 = gyroid(X, Y, Z, 0, f);
% 
% 
% 
% obj3 = intersection(obj1, -obj2);



% Implict cuboid
obj4 = implicit_cuboid(X, Y, Z, 0, 0, 0, 0.5, 0.5, 0.5);



% % Intersection of Gyroid and cuboid
% f_plot = intersection(obj3, obj4);
% 
% 
% f_plot2 = intersection(obj5, obj4);

shape_num = 2;

for n = 1:length(ts_ds)
    t1 = ts_ds(n);
    t2 = -t1;
    t3 = ts_ss(n);

switch shape_num
    case 1
        % Primitive surface
        [obj6,azel] = primitive(X, Y, Z, t1, f);
        obj7 = primitive(X, Y, Z, t2, f);
        obj8 = primitive(X, Y, Z, 0, f);
        obj8 = -obj8;
        
    case 2
        % Gyroid surface
        [obj6,azel] = gyroid(X, Y, Z, t1, f);
        obj7 = gyroid(X, Y, Z, t2, f);
        obj8 = gyroid(X, Y, Z, t3, f);
        
    case 3
        % Diamond surface
        [obj6,azel] = diamond(X, Y, Z, t1, f);
        obj7 = diamond(X, Y, Z, t2, f);
        obj8 = diamond(X, Y, Z, 0, f);
        
    case 4
        % Neovius surface
        [obj6,azel] = neovius(X, Y, Z, t1, f);
        obj7 = neovius(X, Y, Z, t2, f);
        obj8 = neovius(X, Y, Z, 0, f);
        obj8 = -obj8;
        
    case 5
        % iwp surface
        t1 = 1;
        t2 = -1;
        [obj6,azel] = iwp(X, Y, Z, t1, f);
        obj7 = iwp(X, Y, Z, t2, f);
        obj8 = iwp(X, Y, Z, 0, f);
        obj8 = -obj8;
        
end
obj9 = intersection(obj6, -obj7);
% obj9 = intersection(obj6, obj7);

% Intersection of Primitive and cuboid
f_plot3 = intersection(obj9, obj4);


f_plot4 = intersection(obj8, obj4);


%% Plotting

% azel = [-41.775690411922781,28.294669324250478];
%another view: [-15.4, 5]; [50 ,30]



color = [0, 1, 1];

% Gyroid

% %double surface
% 
% figure;
% h1 = isosurface (X, Y, Z, f_plot, 0) ;
% 
% 
% 
% patch(h1 ,'EdgeColor', 'none', 'FaceColor' , color, 'FaceAlpha', 1);
% camlight
% axis equal
% grid on
% set(gca,'XColor', 'none','YColor','none')
% set(gca,'xtick',[])
% set(gca,'visible','off')
% view(azel)
% 
% 
% 
% %single surface
% figure;
% h2 = isosurface (X, Y, Z, f_plot2, 0) ;
% 
% 
% 
% patch(h2 ,'EdgeColor', 'none','FaceColor' , color, 'FaceAlpha', 1);
% camlight
% axis equal
% grid on
% set(gca,'XColor', 'none','YColor','none')
% set(gca,'xtick',[])
% set(gca,'visible','off')
% view(azel)


% Primitive

%double surface

figure;
h3 = isosurface (X, Y, Z, f_plot3, 0) ;



patch(h3 ,'EdgeColor', 'none', 'FaceColor' , color, 'FaceAlpha', 1);
camlight(-45,0)
axis equal
set(gca,'XColor', 'none','YColor','none')
set(gca,'xtick',[])
set(gca,'visible','off')
view(azel)



%single surface
figure;
h4 = isosurface (X, Y, Z, f_plot4, 0) ;



patch(h4 ,'EdgeColor', 'none','FaceColor' , color, 'FaceAlpha', 1);
camlight(-45,0)
axis equal
set(gca,'XColor', 'none','YColor','none')
set(gca,'xtick',[])
set(gca,'visible','off')
view(azel)

end

%% Nested primitive geometry functions
function [output, azel] = gyroid(X,Y,Z,t,f)

azel = [-51.862784546384958,12.426912350032737];

output = sin (f*X).* cos (f*Y) + sin (f*Y).* cos (f*Z) + sin (f*Z).* cos (f*X) - t;

end



function output = implicit_cuboid(x,y,z,x1 ,y1 ,z1 ,A,B,C)



output = max( abs (x- x1)/A, abs (y- y1)/B); %x1 , y1 ,z1 = offset values
output = max( output , abs (z - z1)/C); %A, B, C = determine relative edge lengths
output = output - 1;

end

function [output, azel] = primitive(X,Y,Z,t,f)

azel = [-51.862784546384958,12.426912350032737];

output = cos(f*X) + cos(f*Y) + cos(f*Z) - t;

end

function [output, azel] = neovius(X,Y,Z,t,f)

azel = [-51.862784546384958,12.426912350032737];

output = 3 * (cos(f*X) + cos(f*Y) + cos(f*Z)) + ...
    4 * cos(f*X) .* cos(f*Y) .* cos(f*Z) - t;

end


function [output, azel] = iwp(X,Y,Z,t,f)

azel = [-51.862784546384958,12.426912350032737];

output = 2 * (cos(f*X) .* cos(f*Y) + ...
         cos(f*Y) .* cos(f*Z) + ...
         cos(f*Z) .* cos(f*X)) - ...
         (cos(2*f*X) + cos(2*f*Y) + cos(2*f*Z)) - t;

end

function [output, azel] = diamond(X,Y,Z,t,f)

azel = [-51.862784546384958,12.426912350032737];

output = sin(f*X) .* sin(f*Y) .* sin(f*Z) + ...
    sin(f*X) .* cos(f*Y) .* cos(f*Z) + ...
    cos(f*X) .* sin(f*Y) .* cos(f*Z) + ...
    cos(f*X) .* cos(f*Y) .* sin(f*Z) - ...
    t;

end

%% Functions for CSG Operations
function output = union(a,b)
output = min(a,b);
end



function output = intersection(a,b)
output = max(a,b);
end



function output = difference(a,b)
output = max(-a,b);
end