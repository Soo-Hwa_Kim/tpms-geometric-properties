function varargout = setupContourPlotInputs(data, geometric_property)
% varargout = setupContourPlotInputs(data, geometric_property)
%----------------------------------------------------------------------------
% setupContourPlotInputs.m takes the raw data and reformats the data for
% contour plots later on by essentially reshapes the data. x and y are the
% offset meshgrid and z is the corresponding geometric property. The
% geometric properties for double surfaces is calculated here. Thickness
% will need to output two sets of data for minimum and maximum thickness.
%
% Inputs:    data                   numeric
%            geometric_property     string - indicates which geometric
%                                   property is being used
%
% Outputs:   varargout              cell
%
% Called by: setupAndGeneratePlots.m
%----------------------------------------------------------------------------

%% Create contour plot inputs

if contains(geometric_property, 'Thickness')

    % Polyfit 2D data
    
    [polyfit_vals, poly_coefficients, poly_tol, poly_degree] = polyFit2DRawData(data(:, 1), data(:, 2), data(:, 3));
    
    data(:, 4) = polyfit_vals;
    
    
    % Create offset (x, y) meshgrids
    
    offsets = unique(data(:, 1:2));
    
    [x, y] = meshgrid(offsets, offsets);
    
    
    % Pre-allocate
    
    zmin = nan(size(x));
    zraw = nan(size(x));
    
    
    % Loop through and fill in z matrix with corresponding thickness value
    % for each offset combination
    
    for n = 1:size(data, 1)
        
        ind1 = find(offsets == data(n, 1));
        
        ind2 = find(offsets == data(n, 2));
                
        zmin(ind1,ind2) = data(n, 4);
        
        zraw(ind1,ind2) = data(n, 3);
        
               
    end
    
    
    % Output
    
    varargout{1} = {x, y, zmin};
    varargout{2} = poly_coefficients;
    varargout{3} = poly_tol;
    varargout{4} = poly_degree;
    varargout{5} = zraw;
    
    
else
    
    % Create the n choose k pair combinations for all offsets
    
    offsets = data(:, 1);
    
    combos = nchoosek(offsets, 2);
    
    
    % Pre-allocate
    
    combo_data_matrix = zeros(length(combos), 3);
    
    combo_data_matrix(:, 1:2) = combos;
    
    
    % Loop through and calculate the double surface volumes or surface
    % areas
    
    for n = 1:length(combos)
        
        offset1 = combos(n,1);
        
        offset2 = combos(n,2);
        
        row1 = find(data(:, 1) == offset1);
        
        row2 = find(data(:, 1) == offset2);
        
        if contains(geometric_property, 'Volume')
            
            combo_data_matrix(n, 3) = abs(data(row1, 2) - data(row2, 2));
            
        elseif contains(geometric_property, 'Surface')
            
            combo_data_matrix(n, 3) = data(row1, 2) + data(row2, 2);
            
        end
        
        
    end
    
    % Polyfit 2D data
    
    [polyfit_vals, poly_coefficients, poly_tol, poly_degree] = polyFit2DRawData(combo_data_matrix(:, 1), ...
        combo_data_matrix(:, 2), combo_data_matrix(:, 3));
    
    combo_data_matrix(:, 4) = polyfit_vals;
    
    
    % Create offset (x, y) meshgrids
    
    [x, y] = meshgrid(offsets, offsets);
    
    
    % Pre-allocate
    
    z = nan(size(x));
    zraw = nan(size(x));
    
    % Loop through and fill in z matrix with corresponding volume or
    % surface area value for each offset combination, otherwise nan
    
    for n = 1:size(combo_data_matrix, 1)
        
        data_n = combo_data_matrix(n, :);
        
        ind1 = find(offsets == data_n(1));
        
        ind2 = find(offsets == data_n(2));
        
        z(ind1,ind2) = data_n(4);
        
        zraw(ind1,ind2) = data_n(3);
        
        
    end
    
    
    % Output
    
    varargout{1} = {x, y, z};
    varargout{2} = poly_coefficients;
    varargout{3} = poly_tol;
    varargout{4} = poly_degree;
    varargout{5} = zraw;
    
end


end