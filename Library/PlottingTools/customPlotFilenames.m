function [fig_name, pdf_name, fig_norm_name, pdf_norm_name, singleordouble] = customPlotFilenames(singleordouble, surfaces, geometric_property1, geometric_property2, operation)
% [fig_name, pdf_name, fig_norm_name, pdf_norm_name, singleordouble] = customPlotFilenames(singleordouble, surfaces, geometric_property1, geometric_property2, operation)
%----------------------------------------------------------------------------
% customPlotFilenames.m creates the file names for output plots (.fig or 
% .pdf) as well as checks if singleordouble is numeric and outputs it as a 
% double
%
% Inputs:    singleordouble         string or numeric or char
%            surfaces               cell of char
%            geometric_property1    char 
%            geometric_property2    char
%            operation              char
%
% Outputs:   fig_name               char
%            pdf_name               char
%            fig_norm_name          char - normalized plot
%            pdf_norm_name          char - normalized plot
%            singleordouble         double
%
% Called by: generateCustomPlots.m
%----------------------------------------------------------------------------



% If input for singleordouble is not numeric change it to numeric

if isletter(singleordouble)
    
    if contains(singleordouble, 'ingle')
        
        singleordouble = 1;
        
    else
        
        singleordouble = 2;
        
    end
    
end


% Create file names

surface_letters = changeSurfaceNameToLetter(surfaces);

geometric_name = changeGeomPropToName(geometric_property1);

if nargin > 3
    
    geometric_name2 = changeGeomPropToName(geometric_property2);
    
    operation_name = changeOperationToName(operation);
    
    fname = [surface_letters{:}, '_', num2str(singleordouble), '_', geometric_name, operation_name, geometric_name2];
    
    fname_norm = [surface_letters{:}, '_', num2str(singleordouble), '_', geometric_name, operation_name, geometric_name2, '_norm'];
    
else
    
    fname = [surface_letters{:}, '_', num2str(singleordouble), '_', geometric_name];
    
    fname_norm = [surface_letters{:}, '_', num2str(singleordouble), '_', geometric_name, '_norm'];
    
end

fig_name = [fname, '.fig'];

pdf_name = [fname, '.pdf'];


fig_norm_name = [fname_norm, '.fig'];

pdf_norm_name = [fname_norm, '.pdf'];


end


function surface_letters = changeSurfaceNameToLetter(surfaces)
% For naming purposes. TPMS names will be represented by first letter.

for m = 1:length(surfaces)
    
    surface = surfaces{m};
    
    if contains(surface, 'rimitive')
        
        surface_letter = 'P';
        
    elseif contains(surface, 'yroid')
        
        surface_letter = 'G';
        
    elseif contains(surface, 'iamond')
        
        surface_letter = 'D';
        
    elseif contains(surface, 'eovius')
        
        surface_letter = 'N';
        
    else
        
        surface_letter = 'I';
        
    end
    
    surface_letters{m} = surface_letter;
    
end


end


function geometric_name = changeGeomPropToName(geometric_property)
% For naming purposes. Geometric properties will be abbreviated.

if contains(geometric_property, 'olume')
    
    geometric_name = 'Vol';
    
elseif contains(geometric_property, 'urfac')
    
    geometric_name = 'SA';
    
elseif contains(geometric_property, 'hick')
    
    geometric_name = 'Th';
    
end


end

function operation_name = changeOperationToName(operation)
% For naming purposes. Operation name will be abbreviated.

if contains(operation, 'ivi')
    
    operation_name = 'div';
    
else
    
    operation_name = 'mult';
    
end

end

