function plot_full_dir = createCustomPlotDirectory
% createCustomPlotDirectory
%----------------------------------------------------------------------------
% createCustomPlotDirectory.m checks whether a directory for custom plots
% exists or not in the Output folder. If it doesn't exist then it creates
% it.
%
% Inputs:    N/A
%
% Outputs:   directory          creates directory if doesn't exist already
%
% Called by: generateCustomPlots.m
%----------------------------------------------------------------------------

% Directory

root_dir = pwd;

plot_path = '/Outputs/CustomPlots/';

plot_full_dir = [root_dir, plot_path];


% Check if exists. If not create!

if ~exist(plot_full_dir, 'dir')
    
    mkdir(plot_full_dir);
    
    addpath(plot_full_dir);
    
end

end