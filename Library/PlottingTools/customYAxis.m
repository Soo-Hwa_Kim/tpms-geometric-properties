function y_axis_label = customYAxis(geometric_property1, operation, geometric_property2)
% y_axis_label = customYAxis(operation, geometric_property1, geometric_property2)
%----------------------------------------------------------------------------
% customYAxis.m creates the y-axis label for custom plots. If the custom
% plot is a combination of two geometric properties this function outputs
% an appropriate label.
%
% Inputs:    operation              string - combination operator
%            geometric_property1    string
%            geometric_property2    string (optional)
%
% Outputs:   y_axis_label           string
%
% Called by: generateCustomPlots.m
%----------------------------------------------------------------------------

% Create y-axis label

if nargin > 2 % If combination
    
    if contains(operation, 'ivi')
        
        operation_sym = ' / ';
        
    else
        
        operation_sym = ' * ';
        
    end
    
    geometric_property_name1 = checkNameForAxis(geometric_property1);
    
    geometric_property_name2 = checkNameForAxis(geometric_property2);
    
    y_axis_label = [geometric_property_name1, operation_sym, geometric_property_name2];
    
else % If no combination
    
    geometric_property_name1 = checkNameForAxis(geometric_property1);
    
    y_axis_label = geometric_property_name1;
    
end

end


function geometric_property = checkNameForAxis(geometric_property)
% Adjusts name for axis to better description

if contains(geometric_property, 'urface')
    
    geometric_property = 'Surface Area';
    
elseif contains(geometric_property, 'olume')
    
    geometric_property = 'Volume Fraction';
    
else
    
    geometric_property = 'Minimum Thickness';
    
end


end