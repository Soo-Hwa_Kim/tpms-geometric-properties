function savePlotFigPdf(fig, dir_fig, dir_pdf, filename_fig, filename_pdf)

    savefig([dir_fig, filename_fig]) %save as fig
    set(fig,'Units', 'Inches');
    pos = get(fig, 'Position');
    set(fig, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)])
    print(fig, [dir_pdf, filename_pdf], '-dpdf', '-r0') %save as pdf

end