function plot3inputs = createLineSegmentPlot3Inputs(array1, array2, lwsize)


x = [array1(:,1) array2(:,1)]';
    
y = [array1(:,2) array2(:,2)]';

z = [array1(:,3) array2(:,3)]';


lw = 'LineWidth';

if nargin < 3
    
    lwsize = 2;
    
end


plot3inputs = {x, y, z, lw, lwsize};



end