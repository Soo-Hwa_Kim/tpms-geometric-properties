function makeContourPlots(alias, file_name)

% Load in data

data = unwrapStructure(load(file_name));


% List of all offsets

all_offsets = unique(data(:, 1:2));


% Create meshgrid of all offset values

[x, y] = meshgrid(all_offsets, all_offsets);


% Retrieve minimum maximum thickness value for each offset combination in
% meshgrid (should produce a triangle matrix where rest should be NaNs)

zmin = nan(size(x));
zmax = nan(size(x));

for n = 1:size(data,1)

   data_n = data(n, :);
   ind1 = find(all_offsets == data_n(1));
   ind2 = find(all_offsets == data_n(2));
   zmin(ind1,ind2) = round(data_n(3), 3);
   zmax(ind1,ind2) = round(data_n(4), 3);
    
end


% Create contour plots

figure;
[Cmin, hmin] = contour(x, y, zmin, 30, 'ShowText', 'on');grid on
hmin.LevelList = round(hmin.LevelList, 3);
xlabel('t_i')
ylabel('t_j')
title(sprintf([alias, ' Minimum Thicknesses']))
savefig([alias, '_min_thickness.fig'])


figure;
[Cmax, hmax] = contour(x, y, zmax, 30, 'ShowText', 'on');grid on
hmax.LevelList = round(hmax.LevelList, 3);
xlabel('t_i')
ylabel('t_j')
title(sprintf([alias, ' Maximum Thicknesses']))
savefig([alias, '_max_thickness.fig'])

end