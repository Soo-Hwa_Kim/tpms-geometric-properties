function setupAndGeneratePlots(SURFACE_DATA)
% setupAndGeneratePlots(SURFACE_DATA)
%----------------------------------------------------------------------------
% setupAndGeneratePlots.m is the overhead function for creating plots. The 
% raw data text files plotted to show the volume, surface area, and minimum
% and maximum thicknesses for solid struts and double surfaces. The
% visualization is turned off and the plots are saved into organized output
% directories. Polynomial fitting coefficients are also output into saved
% directories.
%
% Inputs:    SURFACE_DATA           struct
%
% Outputs:   plots                  saved pdf and fig files
%            txt files              saved txt files of fitting coefficients
%
% Called by: TPMS_Analysis.m
%----------------------------------------------------------------------------

%% Volume

% Import data and set up for plotting

volume_raw_data = dlmread(SURFACE_DATA.filenames.raw_data_volume);

volume_raw_data = checkOffsetRange(volume_raw_data);

[volume_contour_plot_inputs, coeff_volume_double, poly_tol_volume_double, ...
    poly_degree_volume_double, raw_data_z] = setupContourPlotInputs(volume_raw_data, ...
    'Volume Fraction');


% Polyfit the data

[polyfit_vals, coeff_volume_single, poly_tol_volume_single, ...
    poly_degree_volume_single] = polyFitRawData(volume_raw_data(:,1), ...
    volume_raw_data(:,2));

volume_polyfit_data = [volume_raw_data(:,1) polyfit_vals];


% Create plots

plotPolyFitAndResiduals(volume_raw_data(:,1), volume_raw_data(:,2), ...
    polyfit_vals, SURFACE_DATA.alias, 'VolumeFraction', ...
    SURFACE_DATA.resid_dir);

plot2DPolyFitAndResiduals(volume_contour_plot_inputs{1}, ...
    volume_contour_plot_inputs{2}, raw_data_z, ...
    volume_contour_plot_inputs{3}, SURFACE_DATA.alias, ...
    'VolumeFraction', SURFACE_DATA.resid_dir);

generatePlots(SURFACE_DATA, volume_contour_plot_inputs, ...
    volume_polyfit_data, 'Volume Fraction');


%% Surface area

% Import data and set up for plotting

surface_area_raw_data = dlmread(SURFACE_DATA.filenames.raw_data_surface_area);

surface_area_raw_data = checkOffsetRange(surface_area_raw_data);

[surface_area_contour_plot_inputs, coeff_surface_area_double, ...
    poly_tol_surface_area_double, poly_degree_surface_area_double, ...
    raw_data_z] = setupContourPlotInputs(surface_area_raw_data, 'Surface Area');


% Polyfit the data

[polyfit_vals, coeff_surface_area_single, poly_tol_surface_area_single, ...
    poly_degree_surface_area_single] = polyFitRawData(surface_area_raw_data(:,1), ...
    surface_area_raw_data(:,2));

surface_area_polyfit_data = [surface_area_raw_data(:,1) polyfit_vals];


% Create plots

plotPolyFitAndResiduals(surface_area_raw_data(:,1), surface_area_raw_data(:,2), ...
    polyfit_vals, SURFACE_DATA.alias, 'SurfaceArea', ...
    SURFACE_DATA.resid_dir);

plot2DPolyFitAndResiduals(surface_area_contour_plot_inputs{1}, ...
    surface_area_contour_plot_inputs{2}, raw_data_z, ...
    surface_area_contour_plot_inputs{3}, SURFACE_DATA.alias, ...
    'SurfaceArea', SURFACE_DATA.resid_dir);

generatePlots(SURFACE_DATA, surface_area_contour_plot_inputs, surface_area_polyfit_data, 'Surface Area');


%% Minimum Thickness

% Solid Strut Thickness

% Import data and set up for plotting

strut_thickness_raw_data = dlmread(SURFACE_DATA.filenames.raw_data_strut_thickness);

strut_thickness_raw_data = checkOffsetRange(strut_thickness_raw_data);

% Polyfit the data

[polyfit_vals, coeff_thickness_single, poly_tol_thickness_single, poly_degree_thickness_single] = polyFitRawData(strut_thickness_raw_data(:,1), strut_thickness_raw_data(:,2));


strut_thickness_polyfit_data = [strut_thickness_raw_data(:,1) polyfit_vals];



% Create plots

plotPolyFitAndResiduals(strut_thickness_raw_data(:,1), strut_thickness_raw_data(:,2), ...
    polyfit_vals, SURFACE_DATA.alias, 'StrutThickness', ...
    SURFACE_DATA.resid_dir);

generatePlots(SURFACE_DATA, [], strut_thickness_polyfit_data, 'Strut Thickness');



% Double Surface Thickness

% Import data and set up for plotting

double_surface_thickness_raw_data = dlmread(SURFACE_DATA.filenames.raw_data_double_surface_thickness);

double_surface_thickness_raw_data = checkOffsetRange(double_surface_thickness_raw_data);

[thickness_contour_plot_inputs, coeff_thickness_double, ...
    poly_tol_thickness_double, poly_degree_thickness_double, ...
    raw_data_z] = setupContourPlotInputs(double_surface_thickness_raw_data, ...
    'Double Surface Thickness');

% Create plots

plot2DPolyFitAndResiduals(thickness_contour_plot_inputs{1}, ...
    thickness_contour_plot_inputs{2}, raw_data_z, ...
    thickness_contour_plot_inputs{3}, SURFACE_DATA.alias, ...
    'DoubleSurfaceThickness', SURFACE_DATA.resid_dir);

generatePlots(SURFACE_DATA, thickness_contour_plot_inputs, double_surface_thickness_raw_data, 'Double Surface Thickness');


%% Save all polynomial coefficient data

% Volume

data_dir = fullfile([SURFACE_DATA.coeff_dir, SURFACE_DATA.filenames.coeff_volume_single]);
dlmwrite(data_dir, coeff_volume_single);

data_dir = fullfile([SURFACE_DATA.coeff_dir, SURFACE_DATA.filenames.coeff_volume_double]);
dlmwrite(data_dir, coeff_volume_double);


% Surface Area

data_dir = fullfile([SURFACE_DATA.coeff_dir, SURFACE_DATA.filenames.coeff_surface_area_single]);
dlmwrite(data_dir, coeff_surface_area_single);

data_dir = fullfile([SURFACE_DATA.coeff_dir, SURFACE_DATA.filenames.coeff_surface_area_double]);
dlmwrite(data_dir, coeff_surface_area_double);


% Minimum Thickness

data_dir = fullfile([SURFACE_DATA.coeff_dir, SURFACE_DATA.filenames.coeff_thickness_single]);
dlmwrite(data_dir, coeff_thickness_single);

data_dir = fullfile([SURFACE_DATA.coeff_dir, SURFACE_DATA.filenames.coeff_thickness_double]);
dlmwrite(data_dir, coeff_thickness_double);


%% Save polynomial fitting tolerances and degrees into log file

headers = {'TPMS', 'GEOM_PROP', 'TOL', 'DEG'};
tpms = {convertCharsToStrings(SURFACE_DATA.alias); convertCharsToStrings(SURFACE_DATA.alias); convertCharsToStrings(SURFACE_DATA.alias)};
geom_props = {"VolFrac"; "SurfArea"; "MinThickness"};
tol1d = [poly_tol_volume_single; poly_tol_surface_area_single; poly_tol_thickness_single];
deg1d = [poly_degree_volume_single; poly_degree_surface_area_single; poly_degree_thickness_single];
table1d = table(tpms, geom_props, tol1d, deg1d, 'VariableNames', headers);
data_dir = fullfile([SURFACE_DATA.polyfit_tol_dir, SURFACE_DATA.filenames.tolerance_1D_log_filename]);
if exist(data_dir, 'file')
    previous_table = readtable(data_dir);
    table1d = [previous_table; table1d];
end
writetable(table1d, data_dir);

tol2d = [poly_tol_volume_double; poly_tol_surface_area_double; poly_tol_thickness_double];
deg2d = [poly_degree_volume_double; poly_degree_surface_area_double; poly_degree_thickness_double];
table2d = table(tpms, geom_props, tol2d, deg2d, 'VariableNames', headers);
data_dir = fullfile([SURFACE_DATA.polyfit_tol_dir, SURFACE_DATA.filenames.tolerance_2D_log_filename]);
if exist(data_dir, 'file')
    previous_table = readtable(data_dir);
    table2d = [previous_table; table2d];
end
writetable(table2d, data_dir);

end