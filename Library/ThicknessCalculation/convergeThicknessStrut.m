function minimum_thickness = convergeThicknessStrut(vertices, fhandle, ghandle, offset_list, faces, thickness_type)
% minimum_thickness = convergeThicknessStrut(vertices, fhandle, ghandle, offset_list, faces, thickness_type)
%----------------------------------------------------------------------------
% convergeThicknessStrut.m calculates a converged minimum thickness per
% offset. The minimum thickness calculation is repeated with an increasing
% number of vertices until minimum thickness values for each offset have
% converged to a tolerance.
%
% Inputs:    vertices           numeric - nx3 initial vertices
%            fhandle                handle - TPMS function handle
%            ghandle                handle - TPMS gradient function handle
%            offset_list            numeric - nx1 offset list
%            faces                  numeric - nx3 initial faces
%            thickness_type         string - specifies either 'Strut' or
%                                   'Double Surface'
%
% Outputs:   offset_min_thickness   numeric - nx2 where the first column
%                                   is offsets and second column is
%                                   minimum thickness values
%
% Called by: generateStrutThicknessData.m
%----------------------------------------------------------------------------

%% Converge minimum thickness for each offset to within a tolerance

% Setup and pre-allocate

is_converged = false;

iteration = 1;

max_iteration = 8;

minimum_thicknesses = zeros(max_iteration, 1);


% Loop until converged

while ~is_converged
    
    
    % Calculate minimum thickness for a set of vertices
    
    minimum_thicknesses(iteration) = calculateThicknessForVertexSet(vertices, ...
        fhandle, ghandle, offset_list, thickness_type);
    
    
    % Test if converged. If converged exit function
    
    if iteration > 2
        
        [min_convergence_test, ~] = testConvergence(minimum_thicknesses((iteration-2):iteration));
        
        if min_convergence_test
            
            break
            
        end
        
    end
    
        
    iteration = iteration + 1;
    
    
    % Development while loop check [TEMPORARY]
    
    if iteration == max_iteration
        
        keyboard
        
    end
    
        
    % If not passed convergence test create more vertices
    
    [faces, vertices] = createNewCorrectedVertices(vertices, faces, fhandle, ghandle, offset_list);
    
    
end


minimum_thickness = minimum_thicknesses(iteration);


end