function [test_results, converged_idx] = testThicknessConvergence(current_thicknesses, previous_thicknesses)
% [test_results, converged_idx] = testThicknessConvergence(thickness, previous_thickness)
%----------------------------------------------------------------------------
% testThicknessConvergence.m tests whether each minimum thickness value is 
% within the set tolerance. The tolerance is a set percentage value to 
% check if the current thickness values have converged relative to prior  
% thickness values.
%
% Inputs:    current_thicknesses    numeric - nx1
%            previous_thicknesses   numeric - nx1
%
% Outputs:   test_results           logical - nx1 pass or not for each
%                                   thickness value
%            converged_idx          numeric - indices of thickness values
%                                   within tolerance
%
% Called by: convergeThicknessStrut.m, convergeThicknessDoubleSurfaces.m
%----------------------------------------------------------------------------

%% Check for convergence

tolerance = 0.0008;

test_results = abs((current_thicknesses ./ previous_thicknesses) - 1) < tolerance;

converged_idx = find(test_results);


end