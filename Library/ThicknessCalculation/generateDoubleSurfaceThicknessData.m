function generateDoubleSurfaceThicknessData(SURFACE_DATA)
% generateDoubleSurfaceThicknessData(SURFACE_DATA)
%----------------------------------------------------------------------------
% generateDoubleSurfaceThicknessData.m is the overhead function for 
% calculating minimum thickness for each offset for double surfaces and 
% saving the data into a text file.
%
% Inputs:    SURFACE_DATA           struct
%
% Outputs:   raw data               saved text file
%
% Called by: generateThicknessData.m
%----------------------------------------------------------------------------

%% Calculate minimum thickness for each surface pair

% Set up starting vertices

field_names = fieldnames(SURFACE_DATA.data);

innermost_vertices = SURFACE_DATA.data.(field_names{end}).initial_vertices;

innermost_faces = SURFACE_DATA.data.(field_names{end}).initial_faces;

thickness_type = 'Double Surface';


% Calculate minimum thickness

offset_combination_min_thicknesses = convergeThicknessDoubleSurfaces(innermost_vertices, ...
    SURFACE_DATA.fhandle, SURFACE_DATA.ghandle, SURFACE_DATA.offset_list, ...
    innermost_faces, thickness_type);


%% Save raw data into text file

filename = SURFACE_DATA.filenames.raw_data_double_surface_thickness;

data_dir = fullfile([SURFACE_DATA.raw_data_dir, filename]);

dlmwrite(data_dir, offset_combination_min_thicknesses);


end