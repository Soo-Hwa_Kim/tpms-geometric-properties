function min_thicknesses = calculateThicknessDoubleSurface(sign_change_step_size, gradients, vertices, fhandle, ghandle, offset_list)
% min_thicknesses = calculateThicknessDoubleSurface(sign_change_step_size, gradients, vertices, fhandle, ghandle, offset_list)
%----------------------------------------------------------------------------
% calculateThicknessDoubleSurface.m calculates a minimum thickness per
% offset combination pair. The offset combinations are all possible 
% combinations of a pair offsets within the offset list. First, the step 
% estimate is then corrected by using the Newton-Raphson method so they 
% are on the surface of the target surface. Then the minimum thickness 
% is obtained for each offset combination pair.
%
% Inputs:    sign_change_step_size  numeric - nx1 sum of steps
%                                   until sign change for each vertex
%            gradients              numeric - nx3 normalized gradient 
%                                   vector per vertex
%            vertices               numeric - nx3 starting vertices
%            fhandle                handle - TPMS function handle
%            ghandle                handle - TPMS gradient function handle
%            offset_list            numeric - mx1 offset list
%
% Outputs:   min_thicknesses        numeric - column of minimum 
%                                   thickness values for each offset pair
%
% Called by: calculateThicknessForVertexSet.m
%----------------------------------------------------------------------------

%% Correct vertices

VERTEX_DATA = getVerticesFromStepEstimate(sign_change_step_size, gradients, vertices, fhandle, ghandle, offset_list);


%% Calculate thickness for offset combinations

min_thicknesses = calculateThicknessForOffsetCombinations(VERTEX_DATA, offset_list);


end