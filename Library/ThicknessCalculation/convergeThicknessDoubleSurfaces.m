function offset_combination_min_thickness = convergeThicknessDoubleSurfaces(vertices, fhandle, ghandle, offset_list, faces, thickness_type)
% [offset_combination_min_thickness, convergence_log] = convergeThicknessDoubleSurfaces(vertices, fhandle, ghandle, offset_list, faces, thickness_type)
%----------------------------------------------------------------------------
% convergeThicknessDoubleSurfaces.m calculates a converged minimum 
% thickness per offset combination. The minimum thickness calculation is 
% repeated with an increasing number of vertices until minimum thickness 
% values for each offset have converged to within a tolerance.
%
% Inputs:    vertices           numeric - nx3 initial vertices
%            fhandle                handle - TPMS function handle
%            ghandle                handle - TPMS gradient function handle
%            offset_list            numeric - nx1 offset list
%            faces                  numeric - nx3 initial faces
%            thickness_type         string - specifies either 'Strut' or
%                                   'Double Surface'
%
% Outputs:   offset_min_thickness   numeric - nx2 where the first column
%                                   is offsets and second column is
%                                   minimum thickness values
%
% Called by: generateDoubleSurfaceThicknessData.m
%----------------------------------------------------------------------------

%% Set up

offset_combinations = nchoosek(offset_list, 2);

unconverged_offset_idx = 1:length(offset_combinations);

starting_vertices_offset = offset_list(end);

iteration = 1;

max_iteration = 7;

minimum_thicknesses = zeros(length(offset_combinations), max_iteration);


%% Loop until all offset combinations have converged

while ~isempty(unconverged_offset_idx)
    
     
    % Calculate minimum thickness for a set of vertices for each offset
    % combination
    
    minimum_thicknesses(:, iteration) = calculateThicknessForVertexSet(vertices, ...
        fhandle, ghandle, offset_list, thickness_type);
    
    
    % Test if converged. If converged exit function
    
    if iteration > 2
        
        [min_convergence_test, min_convergence_test_idx] = testConvergence(minimum_thicknesses(:,(iteration-2):iteration));
        
        if all(min_convergence_test)
            
            break
            
        else
            
            unconverged_offset_idx = setdiff(unconverged_offset_idx, min_convergence_test_idx);
            
        end
        
    end
    
    
    iteration = iteration + 1;
    
    
    % Development while loop check [TEMPORARY]
    
    if iteration == max_iteration
        
        offset_combination_min_thickness = [offset_combinations, minimum_thicknesses(:, iteration)];
        
        keyboard
        
    end
    
    
    % Create more vertices to calculate minimum thickness
    
    [faces, vertices] = createNewCorrectedVertices(vertices, faces, fhandle, ghandle, starting_vertices_offset);
    
 
end


% Output

offset_combination_min_thickness = [offset_combinations, minimum_thicknesses(:, iteration)];


end