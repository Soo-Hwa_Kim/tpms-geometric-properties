function min_thickness = calculateThicknessStrut(sign_change_step_size, gradients, vertices, fhandle, ghandle, offset)
% min_thickness = calculateThicknessStrut(sign_change_step_size, gradients, vertices, fhandle, ghandle, offset)
%----------------------------------------------------------------------------
% calculateThicknessStrut.m calculates a minimum thickness. The step 
% estimate is then corrected by using the Newton-Raphson method so they 
% are on the surface of the other side and then the minimum thickness 
% is obtained.
%
% Inputs:    sign_change_step_size  numeric - nx1 sum of steps
%                                   until sign change for each vertex
%            gradients              numeric - nx3 normalized gradient 
%                                   vector per vertex
%            vertices               numeric - nx3 starting vertices
%            fhandle                handle - TPMS function handle
%            ghandle                handle - TPMS gradient function handle
%            offset_list            numeric - singular offset value
%
% Outputs:   min_thickness          numeric 
%
% Called by: calculateThicknessForVertexSet.m
%----------------------------------------------------------------------------

%% Correct vertices

found_vertices = vertices + sign_change_step_size .* gradients;

found_vertices = correctVertices(found_vertices, fhandle, ghandle, offset);


%% Calculate thicknesses and obtain the minimum

thicknesses = calculateDistance(vertices, found_vertices);

valid_thicknesses = thicknesses(~isnan(thicknesses));

min_thickness = min(valid_thicknesses);


end