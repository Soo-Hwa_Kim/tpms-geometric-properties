function VERTEX_DATA = getVerticesFromStepEstimate(sign_change_step_size, gradients, vertices, fhandle, ghandle, offset_list)
% VERTEX_DATA = getVerticesFromStepEstimate(sign_change_step_size, gradients, vertices, fhandle, ghandle, offset_list)
%----------------------------------------------------------------------------
% getVerticesFromStepEstimate.m corrects vertices for each step estimate.
%
% Inputs:    sign_change_step_size  numeric - nx1 sum of steps
%                                   until sign change for each vertex
%            gradients              numeric - nx3 normalized gradient 
%                                   vector per vertex
%            vertices               numeric - nx3 starting vertices
%            fhandle                handle - TPMS function handle
%            ghandle                handle - TPMS gradient function handle
%            offset_list            numeric - mx1 offset list
%
% Outputs:   VERTEX_DATA            struct - corrected vertices
%
% Called by: calculateThicknessDoubleSurface.m
%----------------------------------------------------------------------------

%% Loop through each offset and correct step estimate

for m = 1:length(offset_list)
    
    offset = offset_list(m);
    
    offset_field_name = getOffsetFieldName(offset);
    
    if m == length(offset_list)
        
        VERTEX_DATA.(offset_field_name).found_vertices = vertices;
        
    else
        
        found_vertices = vertices + sign_change_step_size(:, m) .* gradients;
        
        found_vertices = correctVertices(found_vertices, fhandle, ghandle, offset_list(m));
        
        VERTEX_DATA.(offset_field_name).found_vertices = found_vertices;
        
    end
    
end


end