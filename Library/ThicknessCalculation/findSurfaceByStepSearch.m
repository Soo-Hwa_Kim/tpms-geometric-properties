function [sign_change_step_size, gradients] = findSurfaceByStepSearch(vertices, fhandle, ghandle, offset_list)
% [sign_change_step_size, gradients] = findSurfaceByStepSearch(vertices, fhandle, ghandle, offset_list)
%----------------------------------------------------------------------------
% findSurfaceByStepSearch.m steps along the "inside" (i.e. where function 
% equates to negative values) starting from initial surface vertices until 
% a sign change occurs on the target ending surface. The sign change is 
% calculated from the implicit function values of the step coordinates and 
% the offset of the target ending surface. The step value is a very small 
% constant value below minimum thickness values.
%
% Inputs:    vertices               numeric - nx3 starting vertices
%            fhandle                handle - TPMS function handle
%            ghandle                handle - TPMS gradient function handle
%            offset_list            numeric - singular ('Strut') or nx1 
%                                   ('Double Surface')
%
% Outputs:   sign_change_step_size  numeric - nx1 sum of steps
%                                   until sign change for each vertex
%            gradients              numeric - nx3 normalized gradient 
%                                   vector per vertex
%
% Called by: calculateThicknessForVertexSet.m
%----------------------------------------------------------------------------

%% Set up and pre-allocate

% Set up step size and maximum steps

step_size = 0.0001;

maximum_iteration = ceil(sqrt(3) / step_size); % body diagonal of unit cell


% If calculating for double surface don't include last offset value since
% that is for the surface of the starting offset vertices

if length(offset_list) > 1
    
    offset_list = offset_list(1:end-1);
    
end


% Calculate normalized gradient vectors for each offset

gradients = -1 * ghandle(vertices(:,1), vertices(:,2), vertices(:,3));

gradients = gradients ./ repmat(calculateVectorMagnitude(gradients), 1, 3);


% Pre-allocate matrix that stores all indices when the first time a sign 
% change occurs for each vertex

sign_change_idx = nan(length(vertices), length(offset_list));


%% Loop through all steps

for n = 1:maximum_iteration
    
    next_step = vertices + n * step_size * gradients;
    
    next_fvals = fhandle(next_step(:,1), next_step(:,2), next_step(:,3));
    
    next_signs = sign(repmat(next_fvals, 1, length(offset_list)) - repmat(offset_list, length(next_fvals), 1));
    
    
    % Check for sign change. Only save if first time sign changes.
    
    if n > 1        
        
        detect_sign_change = find(next_signs ~= previous_signs);
        
        new_sign_changes = find(isnan(sign_change_idx));
        
        sign_change_idx(intersect(new_sign_changes, detect_sign_change)) = n;
        
    end
    
    previous_signs = next_signs;
    
end


%% Sum of steps taken until sign change

sign_change_step_size = sign_change_idx * step_size;


end