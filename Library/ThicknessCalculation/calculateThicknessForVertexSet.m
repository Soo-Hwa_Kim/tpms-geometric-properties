function min_thicknesses = calculateThicknessForVertexSet(vertices, fhandle, ghandle, offset_list, thickness_type)
% offset_min_thicknesses = calculateThicknessForVertexSet(vertices, fhandle, ghandle, offset_list, faces, thickness_type)
%----------------------------------------------------------------------------
% calculateThicknessForVertexSet.m calculates a minimum thickness per
% offset. The minimum thickness calculation is done in two stages. First a
% course estimate of the thickness is done by stepping along the normals of
% the initial vertices. This estimate is then corrected using
% Newton-Raphson method so the step location is on the surface and then 
% the minimum thicknesses are obtained.
%
% Inputs:    vertices               numeric - nx3 starting vertices
%            fhandle                handle - TPMS function handle
%            ghandle                handle - TPMS gradient function handle
%            offset_list            numeric - singular ('Strut') or nx1 
%                                   ('Double Surface')
%            thickness_type         string - specifies either 'Strut' or
%                                   'Double Surface'
%
% Outputs:   min_thicknesses        numeric - variable size. Either
%                                   singular or vector. Singular if strut 
%                                   and vector corresponding to all offset
%                                   combinations
%
% Called by: convergeThicknessStrut.m, convergeThicknessDoubleSurfaces.m
%----------------------------------------------------------------------------

%% Course estimate of surface by step search

[sign_change_step_size, gradients] = findSurfaceByStepSearch(vertices, fhandle, ghandle, offset_list);


%% Correct step search length and obtain minimum thicknesses

% Calculate minimum thickness depending on whether Strut or Double Surface

if contains(thickness_type, 'Strut')
    
    min_thicknesses = calculateThicknessStrut(sign_change_step_size, gradients, vertices, fhandle, ghandle, offset_list);
    
else % Double Surface
    
    min_thicknesses = calculateThicknessDoubleSurface(sign_change_step_size, gradients, vertices, fhandle, ghandle, offset_list);
    
end


end