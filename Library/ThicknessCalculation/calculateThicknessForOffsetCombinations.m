function min_thicknesses = calculateThicknessForOffsetCombinations(VERTEX_DATA, offset_list)
% min_thicknesses = calculateThicknessForOffsetCombinations(VERTEX_DATA, offset_list)
%----------------------------------------------------------------------------
% calculateThicknessForOffsetCombinations.m calculates a minimum thickness per
% offset combination pair. The offset combinations are all possible 
% combinations of a pair offsets within the offset list.
%
% Inputs:    VERTEX_DATA            struct - corrected vertices
%            offset_list            numeric - mx1 offset list
%
% Outputs:   offset_min_thicknesses numeric - mx3 where the first two column
%                                   columns are offset combination pairs 
%                                   and third column is the minimum 
%                                   thickness values.
%
% Called by: calculateThicknessDoubleSurface.m
%----------------------------------------------------------------------------

%% Set up and pre-allocate

% All possible offset combination pairs

offset_combinations = nchoosek(offset_list, 2);


% Pre-allocate

min_thicknesses = nan(length(offset_combinations), 1);


%% Loop through all offset combinations and obtain minimum thickness

for n = 1:length(offset_combinations)
    
    % Offset 1
    
    offset1 = offset_combinations(n, 1);
    
    offset1_field_name = getOffsetFieldName(offset1);
    
    offset1_found_vertices = VERTEX_DATA.(offset1_field_name).found_vertices;
    
    
    % Offset 2
    
    offset2 = offset_combinations(n, 2);
    
    offset2_field_name = getOffsetFieldName(offset2);
    
    offset2_found_vertices = VERTEX_DATA.(offset2_field_name).found_vertices;
    
    
    % Calculate minimum thickness
    
    thicknesses = calculateDistance(offset1_found_vertices, offset2_found_vertices);
    
    valid_thicknesses = thicknesses(~isnan(thicknesses));
    
    min_thicknesses(n) = min(valid_thicknesses);
    
    
end


end