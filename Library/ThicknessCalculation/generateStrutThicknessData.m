function generateStrutThicknessData(SURFACE_DATA)
% generateStrutThicknessData(SURFACE_DATA)
%----------------------------------------------------------------------------
% generateStrutThicknessData.m is the overhead function for calculating 
% minimum thickness for each offset for struts and saving the data into a 
% text file.
%
% Inputs:    SURFACE_DATA           struct
%
% Outputs:   raw data               saved text file
%
% Called by: generateThicknessData.m
%----------------------------------------------------------------------------

%% Calculate minimum thickness for each offset

% Setup and pre-allocate

field_names = fieldnames(SURFACE_DATA.data);

strut_offset_min_thicknesses = nan(length(SURFACE_DATA.offset_list), 2);

strut_offset_min_thicknesses(:, 1) = SURFACE_DATA.offset_list;


% Loop through each offset and calculate minimum thickness

for n = 1:length(SURFACE_DATA.offset_list)  
    
    offset = SURFACE_DATA.offset_list(n);
    
    initial_vertices = SURFACE_DATA.data.(field_names{n}).initial_vertices;
    
    initial_faces = SURFACE_DATA.data.(field_names{n}).initial_faces;

    strut_offset_min_thicknesses(n, 2) = convergeThicknessStrut(initial_vertices, ...
        SURFACE_DATA.fhandle, SURFACE_DATA.ghandle, offset, initial_faces, 'Strut');

    
end


%% Intermediate check [TEMPORARY]

% figure
% plot(strut_offset_min_thicknesses(:,1),strut_offset_min_thicknesses(:,2),'LineWidth',2)
% xlabel('Offset')
% ylabel('Minimum Thickness')
% title(SURFACE_DATA.alias)


%% Save raw data into text file

filename = SURFACE_DATA.filenames.raw_data_strut_thickness;

data_dir = fullfile([SURFACE_DATA.raw_data_dir, filename]);

dlmwrite(data_dir, strut_offset_min_thicknesses);


end